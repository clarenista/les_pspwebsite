<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/test', function(){
// 	return view('welcome');
// });

Route::get('/', "HomeController@index");

Route::prefix('auth')->group(function () {
    // Route::post('register', 'AuthController@register');
    // Route::get('refresh', 'AuthController@refresh');
    // Route::group(['middleware' => 'auth:api'], function(){
    //     Route::get('user', 'AuthController@user');
    //     Route::post('logout', 'AuthController@logout');
    // });
    Route::post('users/passwordReset', 'UserController@resetPassword');
    Route::get('users/checkIfPasswordResetKeyExist/{prkey}', 'UserController@checkprkey');
    Route::post('users/passwordResetSave', 'UserController@passwordResetSave');
    
});

Route::get('/about-us', "AboutController@index");

Route::get('/leadership/executiveofficers', "LeadershipController@executiveofficers");
Route::get('/leadership/boardofgovernors', "LeadershipController@boardofgovernors");
Route::get('/leadership/boardofpathology', "LeadershipController@boardofpathology");
Route::get('/leadership/regionalchapters', "LeadershipController@regionalchapters");

Route::get('/councilAndCommittees', "CommitteesController@index");

Route::get('/accreditation', "AccreditationController@index");

Route::get('/discussions', "DiscussionsController@index");
Route::get('/discussions/{id}', "DiscussionsController@show");
Route::post('/discussions/{id}', "DiscussionsController@saveComment");

Route::get('/news', "NewsController@index");
Route::get('/news/{id}', "NewsController@show");


Route::post('/cms/login' , 'UsersController@login');
Route::post('/login', 'AuthController@login');
Route::get('/admin/login', 'Admin\AuthController@adminlogin')->name('login');
Route::post('/admin/login', 'Admin\AuthController@adminlogingin');
Route::get('/admin/logout', 'Admin\AuthController@logout');

Route::get('/admin/users', 'Admin\UserController@index')->middleware('auth')->name('users');
Route::get('/admin/createAdmin', 'Admin\UserController@createAdmin')->middleware('auth')->name('createAdmin');
Route::post('/admin/createAdmin', 'Admin\UserController@storeAdmin')->middleware('auth')->name('storeAdmin');
Route::get('/admin/users/export', 'Admin\UserController@export')->middleware('auth')->name('exportUsers');
Route::get('/admin/users/{id}', 'Admin\UserController@show')->middleware('auth')->name('showUser');
Route::get('/admin/users/{id}/evaluations', 'Admin\UserController@showEval')->middleware('auth')->name('showEval');
Route::post('/admin/users/{id}', 'Admin\UserController@update')->middleware('auth');
Route::get('/admin/users/{id}/paydues', 'Admin\PayDueController@index')->middleware('auth');
Route::get('/admin/users/{id}/paydues/{pd_id}/approve', 'Admin\PayDueController@approve')->middleware('auth')->name('pd_approve');
Route::get('/admin/users/{id}/paydues/{pd_id}/revert', 'Admin\PayDueController@approve_revert')->middleware('auth')->name('pd_approve_revert');
Route::get('/admin/evaluations', 'Admin\EvaluationController@index')->middleware('auth')->name('evaluations');
Route::get('/admin/applications', 'Admin\ApplicationController@index')->middleware('auth')->name('applications');
Route::get('/admin/lectures/{id}/updateIsWatching', 'Admin\LectureController@updateIsWatching')->middleware('auth')->name('updateIsWatching');
Route::resource('/admin/lectures', 'Admin\LectureController', ['as' => 'admin'])->middleware('auth');


// Route::get('/membership/{any?}', function(){
// 	return view('welcome');
// });

Route::get('membership/{vue_capture?}', "MemberController@index")->where('vue_capture', '[\/\w\.-]*');