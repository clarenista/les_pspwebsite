<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('admin/login' , 'UsersController@login');
Route::get('users/searchUser/{query}' , 'UsersController@searchUser');

Route::post('users/save' , 'UsersController@saveUser');


// members
Route::get('members/{id}' , 'MemberController@show');
Route::post('members/update' , 'MemberController@update');


// leadership *EOS
Route::get('leadership/executiveOfficer' , 'LeadershipController@getAllExecOfficer');
Route::get('leadership/officer/{id}' , 'LeadershipController@getOfficerDetails');
Route::get('leadership/officer/{id}/delete' , 'LeadershipController@deleteOfficer');
Route::post('leadership/saveNewOfficer' , 'LeadershipController@saveOfficer');
Route::post('leadership/updateOfficer' , 'LeadershipController@updateOfficer');

// leadership *BOG
Route::get('leadership/boardofgovernors' , 'LeadershipController@getBoards');
Route::get('leadership/boardofgovernors/{id}' , 'LeadershipController@getGovDetails');
Route::get('leadership/boardofgovernors/{id}/delete' , 'LeadershipController@deleteGov');
Route::post('leadership/saveNewGov' , 'LeadershipController@saveGov');
Route::post('leadership/updateGov' , 'LeadershipController@updateGov');

// leadership *BOP
Route::get('leadership/boardofpathology' , 'LeadershipController@getPatho');
Route::get('leadership/boardofpathology/{id}' , 'LeadershipController@getPathoDetails');
Route::get('leadership/boardofpathology/{id}/delete' , 'LeadershipController@deletePatho');
Route::post('leadership/saveNewPatho' , 'LeadershipController@savePatho');
Route::post('leadership/updatePatho' , 'LeadershipController@updatePatho');

// leadership *Regional Leaders
Route::get('leadership/regionalchapter' , 'LeadershipController@regionalLeader');
Route::get('leadership/regionalchapter/{id}' , 'LeadershipController@getRegionalLeaderDetails');
Route::get('leadership/regionalchapter/{id}/delete' , 'LeadershipController@deleteRegionalLeader');
Route::post('leadership/saveNewRegionalLeader' , 'LeadershipController@saveRegionalLeader');
Route::post('leadership/updateRegionalLeader' , 'LeadershipController@updateRegionalLeader');

Route::get('leadership/regions/{id}' , 'LeadershipController@getRegionDetails');
Route::get('leadership/regional-leader/{id}/delete' , 'LeadershipController@deleteLeader');
Route::post('leadership/saveNewRegion' , 'LeadershipController@saveRegion');
Route::get('leadership/regions' , 'LeadershipController@allRegions');

// leadership *Committees
Route::get('leadership/boardofpathology' , 'LeadershipController@getPatho');
Route::get('leadership/boardofpathology/{id}' , 'LeadershipController@getPathoDetails');
Route::get('leadership/boardofpathology/{id}/delete' , 'LeadershipController@deletePatho');
Route::post('leadership/saveNewPatho' , 'LeadershipController@savePatho');
Route::post('leadership/updatePatho' , 'LeadershipController@updatePatho');

// News
Route::get('news/all', 'NewsController@fetchAll');
Route::post('news/save', 'NewsController@saveNews');
Route::get('news/{id}', 'NewsController@showNewsDetails');
Route::post('news/update', 'NewsController@updateNews');
Route::get('news/{id}/delete', 'NewsController@deleteNews');

// Councils & Committees
Route::get('councils/all', 'CouncilController@fetchAll');
Route::post('councils/save' , 'CouncilController@store');
Route::get('councils/{id}' , 'CouncilController@show');
Route::post('councils/update' , 'CouncilController@update');
Route::get('councils/{id}/delete' , 'CouncilController@delete');

Route::get('committees/all', 'CommitteesController@fetchAll');
Route::get('committees/{id}' , 'CommitteesController@getCommitteeDetails');
Route::get('committees/{id}/delete' , 'CommitteesController@deleteCommittee');
Route::post('committees/saveNewCommittees' , 'CommitteesController@saveCommittees');
Route::post('committees/updateCommittees' , 'CommitteesController@updateCommittees');

// Accreditation
Route::get('accreditation/all' , 'AccreditationController@fetchAll');
Route::post('accreditation/saveNewAccredited' , 'AccreditationController@saveAccredited');
Route::get('accreditation/{id}' , 'AccreditationController@getAccredited');
Route::post('accreditation/updateAccredited' , 'AccreditationController@updateAccredited');
Route::get('accreditation/{id}/delete' , 'AccreditationController@deleteAccredited');

// Discussions
Route::get('discussions/all', 'DiscussionsController@fetchAll');
Route::post('discussions/save', 'DiscussionsController@saveDiscussion');
Route::get('discussions/{id}', 'DiscussionsController@showDiscussionDetails');
Route::post('discussions/update', 'DiscussionsController@updateDiscussion');
Route::get('discussions/{id}/delete', 'DiscussionsController@deleteDiscussion');

// Media
Route::get('media', 'MediaController@index');
Route::post('media/save', 'MediaController@save');

// Lectures
Route::get('lectures/all', 'LectureController@fetchAll');
Route::post('lectures/save', 'LectureController@saveLecture');
Route::get('lectures/fetch/{id}', 'LectureController@showLectureDetails');
Route::get('lectures/getPurchasedDetails/{lecture_id}', 'LectureController@getPurchasedDetails');
Route::get('lectures/markAsPaid/{member_id}', 'LectureController@markAsPaid');

// Banner
Route::get('banner/banner_details', 'BannerController@get_banner');
Route::post('banner/update', 'BannerController@update');

// Video
Route::get('video/video_details', 'VideoController@get_video');
Route::post('video/update', 'VideoController@update');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Route::prefix('auth')->group(function () {
//     Route::post('register', 'AuthController@register');
//     Route::get('refresh', 'AuthController@refresh');
//     Route::group(['middleware' => 'auth:api'], function(){
//         Route::get('user', 'AuthController@user');
//         Route::post('logout', 'AuthController@logout');
//     });
//     Route::post('users/passwordReset', 'UserController@resetPassword');
//     Route::get('users/checkIfPasswordResetKeyExist/{prkey}', 'UserController@checkprkey');
//     Route::post('users/passwordResetSave', 'UserController@passwordResetSave');
    
// });


Route::get('user', 'UserController@showUser');
Route::post('users/update', 'UserController@save');
Route::group(['middleware' => 'auth:api'], function(){
    // Cert Application
    Route::post('application/submit', 'CertApplicationController@apply');
    Route::get('application/good-standing', 'GoodStandingController@fetch');
    Route::post('application/good-standing/payment', 'GoodStandingController@uploadFileReceipt');

    // User Affiliation
    Route::post('affiliation/save', 'AffiliationController@save');
    Route::post('affiliation/update', 'AffiliationController@update');
    Route::delete('affiliation/{affiliation_id}/delete', 'AffiliationController@destroy');

    // User Credentials
    Route::post('credentials/save' , 'CredentialController@save');
    Route::get('credentials/show/{credtype}' , 'CredentialController@show');
    Route::get('credentials/psp-cert' , 'CredentialController@pspCert');

    // User Pay Dues
    Route::post('paydues/save', 'PaydueController@save' );
    Route::get('paydues/{year}', 'PaydueController@getPaidDue');
    Route::post('paydues/uploadReceipt', 'PaydueController@uploadReceipt');

    // Lists of lectures
    Route::get('lectures', 'LectureController@index');
    Route::get('lectures/{id}', 'LectureController@show');
    Route::post('lectures/{id}/add-view', 'LectureController@addViewer');
    Route::post('lectures/{id}/remove-view', 'LectureController@removeViewer');
    Route::post('lectures/{id}/viewer-count', 'LectureController@viewersCount');

    Route::get('lectures/{id}/questions', 'LectureController@questions');
    Route::get('lectures/{id}/fetchExamTrials', 'LectureController@fetchExamTrials');

    Route::post('lectures/exam/submitAnswers', 'LectureController@saveAnswers');

    // new lecture payment
    Route::post('lectures/pay', 'LectureController@pay' );
    // Route::get('lectures/{id}', 'LectureController@getLecture' );
    Route::get('purchasedLecture/{id}', 'LectureController@getPurchasedLecture' );
    Route::post('lectures/uploadReceipt', 'LectureController@uploadReceipt');
    Route::post('purchasedLecture/addView', 'LectureController@addView' );
    
    Route::get('evaluations/', 'EvaluationController@show' );
    Route::post('evaluation/save', 'EvaluationController@store' );
    




    Route::get('users', 'UserController@index');
    Route::get('users/{id}', 'UserController@show')->middleware('isAdminOrSelf');
});

