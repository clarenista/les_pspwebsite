<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Committee;

class Council extends Model
{
    protected $table = 'councils';

    function committees(){
    	return $this->hasMany(Committee::class, 'council_id', 'id');
    }
}
