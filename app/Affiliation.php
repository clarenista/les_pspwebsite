<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Affiliation extends Model
{
    protected $table = 'm_hospital_affilations';
    protected $fillable = ['hospital_name', 'position', 'hospital_address'];
}
