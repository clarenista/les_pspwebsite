<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LectureViewer extends Model
{
    //
    protected $fillable = ['user_id', 'lecture_id', 'is_watching'];

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
