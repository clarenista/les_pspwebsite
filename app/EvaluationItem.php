<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluationItem extends Model
{
    protected $fillable = ['description', 'day'];

    public function evaluation(){
        return $this->hasOne('App\Evaluation', 'id', 'evaluation_item_id');
    }

    public function user_evaluations(){
        return $this->hasMany('App\Evaluation', 'evaluation_item_id',  'id');
    }
    
    public function user_radio_rate_eval(){
        return $this->hasMany('App\Evaluation', 'evaluation_item_id',  'id')->where('type', 'radio_rate');

    }
    public function user_content_eval(){
        return $this->hasMany('App\Evaluation', 'evaluation_item_id',  'id')->where('type', 'content');
    }
    public function user_adherence_eval(){
        return $this->hasMany('App\Evaluation', 'evaluation_item_id',  'id')->where('type', 'adherence');
    }
    
}
