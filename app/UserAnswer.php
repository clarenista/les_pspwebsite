<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LectureQuestion;
use App\QuestionChoice;

class UserAnswer extends Model
{
    protected $table = 'l_member_answers';

    function question_details(){
    	return $this->hasOne(LectureQuestion::class, 'id', 'l_question_id');
    }

    function choice_details(){
    	return $this->hasOne(QuestionChoice::class, 'id' , 'answer_choice_id');
    }
}
