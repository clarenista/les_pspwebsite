<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RegionalLeader;

class Region extends Model
{
    protected $table = 'regions';

    public function leaders(){
    	return $this->hasMany(RegionalLeader::class, 'region_id', 'id')->orderBy('row_position', 'asc');
    }
}
