<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\QuestionChoice;

class LectureQuestion extends Model
{
    protected $table = 'l_questions';

    function choices(){
    	return $this->hasMany(QuestionChoice::class, 'l_question_id', 'id');
    }
}
