<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExecutiveOfficer extends Model
{
    protected $table = 'executive_officers';
}
