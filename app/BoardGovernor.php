<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardGovernor extends Model
{
    protected $table = 'board_of_governors';
}
