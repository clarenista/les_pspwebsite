<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $fillable = ['user_id', 'evaluation_item_id', 'type', 'rate'];
    public function item(){
        return $this->belongsTo('App\EvaluationItem', 'evaluation_item_id' , 'id');
    }
}
