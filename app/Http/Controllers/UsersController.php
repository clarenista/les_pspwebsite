<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    function login(Request $request){
        // return hash('sha256', Str::random(80));
        $credentials = $request->only('username', 'password');
        $user = User::where('username', $request->username)->where('role', 0)->first();
        if($user){
            
            if(Hash::check($request->password, $user->password)){
                return response()->json([
                    'status' => 'success',
                    'user' => $user,
                ]);
            }else{
                
                return response('',403);
            }
        }else{
            return response('',403);
        }

    }

    function searchUser($query){
        $users = User::where('firstName', 'like', '%'.$query.'%')->orWhere('lastName', 'like', '%'.$query.'%')->get();
        return response()->json([
            'users' => $users,
        ]);
    }

    function saveUser(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,emailAddress',
            'username' => 'required|unique:users,email',
        ]);        
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->messages()]);
        }else{
            $user = new User();
            $user->email = $request->username;
            $user->emailAddress = $request->email;
            $user->firstName = $request->firstName;
            $user->lastName = $request->lastName;
            $user->password = '$2y$10$Jy4BgwI6FDn9aFdCnQzyBej0adl09FUUwQgICf1YAAfXXk0DIhV1e';
            $user->save();
            return response()->json(['status' => 'success']);
        }        
    }


}
