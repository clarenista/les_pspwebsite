<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CertApplication;

class GoodStandingController extends Controller
{

    function fetch(){
        $application = CertApplication::where('user_id', request()->user()->id)->where('type', 'good_standing')->first();
        return response()->json(['application' => $application]);
    }
    
    public function uploadFileReceipt(){
        $application = CertApplication::where('user_id', request()->user()->id)->where('type', 'good_standing')->first();
        $this->uploadFile($application, 'file', 'receipt_path');
        return response()->json(['status' => 'ok']);
        
    }
}
