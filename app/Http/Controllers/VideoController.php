<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideoController extends Controller
{
    function get_video(){
    	$video_details = Video::find(1);
    	return response()->json(['video_details' => json_decode($video_details)]);
    }

    function update(Request $request){
    	$video = Video::find(1);
    	$video->title = $request->title;
    	$video->url = $request->url;
    	$video->save();

    }    
}
