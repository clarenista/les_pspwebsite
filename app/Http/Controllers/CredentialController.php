<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Credential;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use \setasign\Fpdi\Fpdi;

class CredentialController extends Controller
{
    function save(Request $request){
        $isCredentialExists = Credential::whereUserId(request()->user()->id)->whereName($request->input('credtype'))->first();

        if($isCredentialExists){
            $this->updateCredential($request);
        }else{
            $this->createNewCredentials($request);
        }
    }

    private function updateCredential($request){
        $credential = Credential::whereUserId(request()->user()->id)->whereName($request->input('credtype'))->first();
        $imageName = $credential->user_id."_".$credential->name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads/credentials'), $imageName);
        $credential->file_path = '/uploads/credentials/'.$credential->user_id."_".$credential->name.'.'.$request->image->getClientOriginalExtension();
        $credential->save();
        return response()->json(['success'=>'You have successfully upload image.']);
    }

    private function createNewCredentials($request){
        $credential = new Credential();
        // /uploads/board-of-pathology/1_Chair.png
        $credential->name = $request->input('credtype');
        $credential->user_id = request()->user()->id;
        // upload
        $imageName = $credential->user_id."_".$credential->name.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads/credentials'), $imageName);

        $credential->file_path = '/uploads/credentials/'.$credential->user_id."_".$credential->name.'.'.$request->image->getClientOriginalExtension();
        $credential->save();
        
        return response()->json(['success'=>'You have successfully upload image.']);
    }

    function show($credtype){
        $credential = Credential::select('file_path')->whereUserId(request()->user()->id)->whereName($credtype)->first();
        return response()->json([
            'credential' => $credential
        ]);
    }

    function pspCert(){
        // dd(request()->user());
        // Create new Landscape PDF
        $name = request()->user()->firstName . ' ' . request()->user()->lastName;
        $serial_no = 'AC2021'.str_pad(request()->user()->id, 4, "0", STR_PAD_LEFT);
        $pdf = new FPDI('l'  ,'pt', 'Letter');

        // Reference the PDF you want to use (use relative path)
        $pagecount = $pdf->setSourceFile( 'uploads/coa.pdf');

        // Import the first page from the PDF and add to dynamic PDF
        $tpl = $pdf->importPage(1);
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true,0);

        // Use the imported page as the template
        $pdf->useTemplate($tpl);

        // adding a Cell using:
        // $pdf->Cell( $width, $height, $text, $border, $fill, $align);
        
        // Name
        $pdf->SetFont('Helvetica', 'B', 30);
        // $pdf->SetTextColor(0,0,0);
        $pdf->SetXY(20, 315); // set the position of the box
        $pdf->Cell(0, 10, utf8_decode($name), 0, 0, 'C'); // add the text, align to Center of cell
        
        // Serial number
        $pdf->SetFont('Helvetica', 'I', 17);
        $pdf->SetTextColor(255,255,255);
        $pdf->SetXY(664, 587);
        $pdf->Cell(1, 10, $serial_no, 0, 0, "L");

        // render PDF to browser
        $pdf->Output();
    }
}
