<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paydue;
use Illuminate\Support\Facades\Auth;

class PaydueController extends Controller
{
    function save(Request $request){
    	$paydue = new Paydue();
    	$paydue->payment_status = 'pending';
    	$paydue->year = $request->input('year');
        if($request->input('psp') == 0){
            $paydue->psp = 'Lifetime Fellow';
        }else if($request->input('psp') == 1500.00){
            $paydue->psp = 'Fellow';
        }else{
            $paydue->psp = 'Diplomate';
        }
    	$paydue->iap = $request->input('iap') > 0 ? 1 : 0;
    	$paydue->psp_circle = $request->input('psp_circle') > 0 ? 1 : 0;
    	$paydue->cgs = $request->input('cgs') > 0 ? 1 : 0;
    	$paydue->total = $request->input('total');
    	$paydue->payment_method = $request->input('payment_method');
    	$paydue->user_id = request()->user()->id;
    	$paydue->save();

        if($request->input('payment_method') == 'ushare'){

           //prepare authentication header parameters: see documentation #authentication section ------------------------------------------------------------
            $merchant_code = 'PSPI';
            $api_id = '09977b8013d6ed67edb5df09d6286ecf';
            $api_secret = 'a1a1081ea986a1195292d0224d527df5';

            $gmt_now = gmdate("M d Y H:i:s", time());
            $gmt_now = strtotime($gmt_now);
            $api_expires = $gmt_now + 28800 + 10800; //GMT + 8 Hours (Philippine Time) + expiration time in secs (in this case 3 hours - 3600*3 = 10800)

            $api_sig = hash_hmac('sha256', $api_expires, $api_secret);

            //prepare fields to populate: see documentation #api-methods-checkouts section -------------------------------------------------------------------
            $fields = array();
            
            $particular_fields = array(
                                'particular_name_1'     =>  'Membership_'.$request->input('year'),

                                'particular_price_1'    =>  $request->input('total'),
                                'particular_quantity_1' =>  '1',
                                'particular_code_1'     =>  'MEMBERSHIP'.$request->input('year'),
                           ); //required
            
            $fields = array_merge($fields, $particular_fields); 

            $autopopulate_fields = array('billing_first_name' => Auth::user()->firstName,
                                         'billing_last_name'  => Auth::user()->lastName); //optional
            
                
            $fields = array_merge($fields, $autopopulate_fields);
            
            $fields_string = '';

            foreach($fields as $key=>$value) 
            { 
                $fields_string .= $key.'='.$value.'&'; 
            }
            $fields_string = rtrim($fields_string,"&");

            //prepare checkouts API call ---------------------------------------------------------------------------------------------------------------------
            $checkouts_url = 'http://api.dev.ubiz.unionbank.com.ph/v1/checkouts.json'; //for PROD environment use https://secured.ubiz.unionbank.com.ph
            // $checkouts_url = 'https://secured.ubiz.unionbank.com.ph'; 
            
            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL, $checkouts_url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('UNIONBANK-UBIZ-APP-ID:              '.$api_id.'', 
                                                       'UNIONBANK-UBIZ-APP-SIGNATURE:       '.$api_sig.'', 
                                                       'UNIONBANK-UBIZ-APP-MERCHANT-CODE:   '.$merchant_code.'', 
                                                       'UNIONBANK-UBIZ-REQUEST-EXPIRES:     '.$api_expires.''));
            curl_setopt($ch,CURLOPT_POST,count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            
            //IMPORTANT: UNCOMMENT for PROD environment
            //Note that even if your data is sent over SSL secured site, a certificate checking is recommended as an added security,
            //by ensuring that your CURL session will not just trust any server certificate
            // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);                       // verify peer's certificate 
            // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);                          // check the existence of a common name and also verify that it matches the hostname provided
            // curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/gd_bundle-g2-g1.crt");     // name of a file holding one or more certificates to verify the peer with. 
                                                                                    // see documentation #before-going-live section for notes regarding certificate file.
            
            $result = curl_exec($ch);   

            if(curl_errno($ch))
            {
                //you can log the curl error
                $message = curl_errno($ch).': '.curl_error($ch);
            }
            else
            {
                //you can log the post information
                $info = curl_getinfo($ch);
                $message = 'POST '. $info['url'].' in '.$info['total_time'].' secs. ' . $info['http_code'] .' - '.$fields_string;

                //process returned JSON response, e.g save response
                $data = json_decode($result);
                
                if($data->meta->response_code == '200')
                {
                    //redirect your customer to UBIZ payment page
                    redirect($data->response->checkout_url);
                    // echo 'Redirect your customer to UBIZ payment page: <br />'.$data->response->checkout_url;
                    // echo $this->session->userdata('userId');
		            return response()->json([
		            	'payment_method' => 'ushare',
		            	'checkout_url' => $data->response->checkout_url
		            ]);                     
                }
                else
                {
                    //capture error and do necessary process
                    //log error details 
                    echo 'Log error details: '.$data->meta->error_type.' | '.$data->meta->error_detail.' | '.$data->meta->error_code;
                    
                    //to force error for testing purposes you can change expires value: $api_expires = $gmt_now - 28800 + 10800;
                }
            }    

            echo '<hr />';
            echo "CURL action: <pre>$message;</pre>";
            echo "CURL raw result: <pre>".print_r($result, true)."</pre>";     
            
            curl_close($ch);      
                        
        }else{
            return response()->json([
            	'payment_method' => 'bpi'
            ]);
        }

    }



    function getPaidDue($year){
        $paydue = Paydue::whereUserId(request()->user()->id)->where('year' , $year)->first();
        return response()->json([
            'paydue' => $paydue
        ]);
        
    }

    function uploadReceipt(Request $request){
        $paydue = Paydue::whereUserId(request()->user()->id)->where('year' , $request->input("year"))->first();
        $imageName = $paydue->user_id."_".$paydue->id.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads/bpireceipt'), $imageName);
        $paydue->bpifilepath = '/uploads/bpireceipt/'.$paydue->user_id."_".$paydue->id.'.'.$request->image->getClientOriginalExtension();
        $paydue->save();
        return response()->json([
            'success'=>'You have successfully upload image.',
            'bpifilepath' => $paydue->bpifilepath
        ]);        

    }
}
