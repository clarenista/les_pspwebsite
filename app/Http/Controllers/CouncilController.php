<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Council;

class CouncilController extends Controller
{
    public function fetchAll(){
    	return response()->json(['councils'=> Council::get()]);
    }

    public function show($id){
    	return response()->json(['council'=> Council::with('committees')->where('id', $id)->first()]);
    }

    public function update(Request $request){
    	$council = Council::find($request->id);
    	$council->name = $request->name;
    	$council->save();
    }    
    public function delete($id){
    	$council = Council::find($id);
    	$council->delete();
    } 
    public function store(Request $request){
    	$council = DB::table('councils')
    				->insert([
    					'name'=> $request->name
    				]);
    }               
}
