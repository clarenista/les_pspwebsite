<?php

namespace App\Http\Controllers;

use App\Lecture;
use App\LectureQuestion;
use App\PurchasedLecture;
use App\QuestionChoice;
use App\UserAnswer;
use App\LectureViewer;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LectureController extends Controller
{

    public function index()
    {
        $lectures = Lecture::all();
        foreach ($lectures as $key => $value) {
            $lectures[$key]->ispurchased = PurchasedLecture::select('payment_status', 'payment_method', 'bpifilepath')->where('user_id' , Auth::user()->id)->where('lecture_id' , $value->id)->first();
        }
        return response()->json([
            'lectures' => $lectures
        ]);        
    }

    function getPurchasedLecture($id){
        return response()->json(['purchased' => PurchasedLecture::with('lecture_details')->whereUserId(Auth::id())->whereLectureId($id)->first()]);
    }

    function getLecture($id){
        return response()->json(['lecture' => Lecture::with('viewers')->find($id) ]);   

    }

    function uploadReceipt(Request $request){
        $plecture = PurchasedLecture::whereUserId(Auth::user()->id)->where('lecture_id' , $request->input("lecture_id"))->first();
        $imageName = $plecture->user_id."_".$plecture->id.'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads/bpireceipt/lecture'), $imageName);
        $plecture->bpifilepath = '/uploads/bpireceipt/lecture/'.$plecture->user_id."_".$plecture->id.'.'.$request->image->getClientOriginalExtension();
        $plecture->save();
        return response()->json([
            'success'=>'You have successfully upload image.',
            'bpifilepath' => $plecture->bpifilepath
        ]);        

    }

    public function pay(Request $request)
    {
        $paylecture = new PurchasedLecture();
        $paylecture->details = $request->input('details');
        $paylecture->lecture_id = $request->input('id');
        $paylecture->user_id = Auth::id();
        $paylecture->payment_status = 'pending';
        $paylecture->payment_method = $request->input('payment_method_selected');
        $paylecture->save();
        if($request->input('payment_method_selected') == 'ushare'){

           //prepare authentication header parameters: see documentation #authentication section ------------------------------------------------------------
            $merchant_code = 'PSPI';
            $api_id = '09977b8013d6ed67edb5df09d6286ecf';
            $api_secret = 'a1a1081ea986a1195292d0224d527df5';

            $gmt_now = gmdate("M d Y H:i:s", time());
            $gmt_now = strtotime($gmt_now);
            $api_expires = $gmt_now + 28800 + 10800; //GMT + 8 Hours (Philippine Time) + expiration time in secs (in this case 3 hours - 3600*3 = 10800)

            $api_sig = hash_hmac('sha256', $api_expires, $api_secret);

            //prepare fields to populate: see documentation #api-methods-checkouts section -------------------------------------------------------------------
            $fields = array();
            
            $particular_fields = array(
                                'particular_name_1'     =>  'Lecture_'.$request->input('id'),

                                'particular_price_1'    =>  $request->input('amount'),
                                'particular_quantity_1' =>  '1',
                                'particular_code_1'     =>  'LECTURE'.$request->input('id'),
                           ); //required
            
            $fields = array_merge($fields, $particular_fields); 

            $autopopulate_fields = array('billing_first_name' => Auth::user()->firstName,
                                         'billing_last_name'  => Auth::user()->lastName); //optional
            
                
            $fields = array_merge($fields, $autopopulate_fields);
            
            $fields_string = '';

            foreach($fields as $key=>$value) 
            { 
                $fields_string .= $key.'='.$value.'&'; 
            }
            $fields_string = rtrim($fields_string,"&");

            //prepare checkouts API call ---------------------------------------------------------------------------------------------------------------------
            $checkouts_url = 'http://api.dev.ubiz.unionbank.com.ph/v1/checkouts.json'; //for PROD environment use https://secured.ubiz.unionbank.com.ph
            
            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL, $checkouts_url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('UNIONBANK-UBIZ-APP-ID:              '.$api_id.'', 
                                                       'UNIONBANK-UBIZ-APP-SIGNATURE:       '.$api_sig.'', 
                                                       'UNIONBANK-UBIZ-APP-MERCHANT-CODE:   '.$merchant_code.'', 
                                                       'UNIONBANK-UBIZ-REQUEST-EXPIRES:     '.$api_expires.''));
            curl_setopt($ch,CURLOPT_POST,count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            
            //IMPORTANT: UNCOMMENT for PROD environment
            //Note that even if your data is sent over SSL secured site, a certificate checking is recommended as an added security,
            //by ensuring that your CURL session will not just trust any server certificate
            //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);                       // verify peer's certificate 
            //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);                          // check the existence of a common name and also verify that it matches the hostname provided
            //curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/path/to/file.crt");     // name of a file holding one or more certificates to verify the peer with. 
                                                                                    // see documentation #before-going-live section for notes regarding certificate file.
            
            $result = curl_exec($ch);   

            if(curl_errno($ch))
            {
                //you can log the curl error
                $message = curl_errno($ch).': '.curl_error($ch);
            }
            else
            {
                //you can log the post information
                $info = curl_getinfo($ch);
                $message = 'POST '. $info['url'].' in '.$info['total_time'].' secs. ' . $info['http_code'] .' - '.$fields_string;

                //process returned JSON response, e.g save response
                $data = json_decode($result);
                
                if($data->meta->response_code == '200')
                {
                    //redirect your customer to UBIZ payment page
                    redirect($data->response->checkout_url);
                    // echo 'Redirect your customer to UBIZ payment page: <br />'.$data->response->checkout_url;
                    // echo $this->session->userdata('userId');
                    return response()->json([
                        'payment_method_selected' => 'ushare',
                        'checkout_url' => $data->response->checkout_url
                    ]);                     
                }
                else
                {
                    //capture error and do necessary process
                    //log error details 
                    echo 'Log error details: '.$data->meta->error_type.' | '.$data->meta->error_detail.' | '.$data->meta->error_code;
                    
                    //to force error for testing purposes you can change expires value: $api_expires = $gmt_now - 28800 + 10800;
                }
            }    

            echo '<hr />';
            echo "CURL action: <pre>$message;</pre>";
            echo "CURL raw result: <pre>".print_r($result, true)."</pre>";     
            
            curl_close($ch);      
                        
        }else{
            return response()->json([
                'payment_method' => 'bpi'
            ]);
        }
    }

    public function show($lecture_id)
    {
        $lecture = Lecture::with('viewers')->find($lecture_id);
        return response()->json([
            'lecture' => $lecture,
            'viewer' => $lecture->viewers()->whereUserId(request()->user()->id)->first()
        ]);
    }

    function addView(Request $request){
        $plecture = PurchasedLecture::find($request->input('id'));
        $plecture->views ++;
        $plecture->save();
        return response()->json([
            'status'=>'success'
        ]);
    }

    function addViewer(Request $request){
        $viewer = new LectureViewer;
        $viewer->updateOrCreate(
            [
            'user_id' => request()->user()->id,
            'lecture_id' => request()->lecture_id,
            ],
            ['is_watching' => 1]
        );
    }


    function removeViewer(Request $request){
        request()->user()->lecture_views()->whereLectureId(request()->lecture_id)->update(['is_watching' => 0]);
        // dd(request()->user()->lecture_views()->whereLectureId(request()->id)->get());
        // dd(request()->all());
    }

    function viewersCount(){
        $count = LectureViewer::where('lecture_id',request()->id)->where('is_watching', 1)->count();
        return $count;


    }

    function questions($lecture_id){
        $questions = Lecture::take(3)->with(['questions' => function($q){return $q->limit(4);}])->find($lecture_id);
        foreach ($questions->questions as $key => $value) {
            $questions->questions[$key]->choices = QuestionChoice::select('id', 'l_question_id', 'choice_value')->where('l_question_id', $value->id)->inRandomOrder()->get();
        }
        return response()->json([
            'lecture' => $questions
        ]);
    }

    function saveAnswers(Request $request){
        $answers = json_decode($request->input('answers'));
        $exam_taken = $request->input('exam_taken');
        $attempts = $exam_taken + 1;
        foreach ($answers as $key => $value) {
            $user_answers = new UserAnswer();
            $user_answers->l_question_id = $key;
            $user_answers->answer_choice_id = $value;
            $user_answers->user_id = Auth::id();
            $user_answers->lecture_id = $request->input('lecture_id');
            $user_answers->attempts = $attempts;
            // $isCorrect[$key] = QuestionChoice::select('id')->where('l_question_id' , $key)->whereCorrectAnswer(1)->first();
            $answers->$key = QuestionChoice::select('correct_answer', 'l_question_id')->where('l_question_id' , $key)->where('id', $value)->first(); 
            $toJson =json_encode($answers);
            $user_answers->save();
        }
        // update purchased the lecture 
        $purchasedLecture = PurchasedLecture::find($request->input('plecture_id'));
        $purchasedLecture->exam_taken += 1;
        $purchasedLecture->save();

        return response()->json(['status' => 'success' , 'isCorrect' => $toJson]);
    }

    function fetchExamTrials($lecture_id){
        // $user_answers = UserAnswer::with('question_details')->with('choice_details')->where('lecture_id', $lecture_id)->where('user_id',Auth::id())->get();

        $user_answers = DB::table('l_member_answers')
                        // ->select('id','attempts','l_question_id','answer_choice_id')
                        ->where('lecture_id', $lecture_id)
                        ->where('user_id',Auth::id())
                        ->groupBy('attempts')
                        ->orderBy('attempts', 'asc')
                        ->get();
        foreach ($user_answers as $key => $value) {
            $user_answers[$key]->questionAnswerDetails = UserAnswer::with('question_details')->with('choice_details')->where('attempts', $value->attempts)->get();
        }

        return response()->json([
            'user_answers' => $user_answers
        ]);

    
    }

    function fetchAll(){
        return response()->json([
            'lectures' => Lecture::all()
        ]);
    }

    function saveLecture(Request $request){
        $lecture  = new Lecture();
        $lecture->title = $request->input('title');
        $lecture->price = $request->input('price');
        $lecture->save();
        $lastIdInserted = Lecture::select('id')->orderBy('id', 'desc')->first();
        $thisLecture = Lecture::find($lastIdInserted->id);
        $id = $thisLecture->id;
        $imageName = $id."_".$request->input('title').'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads/lectures'), $imageName);
        $thisLecture->file_path = '/uploads/lectures/'.$id."_".$request->input('title').'.'.$request->image->getClientOriginalExtension();
        $thisLecture->save();        
        return response()->json(['status'=>'success']);         
    }

    function showLectureDetails($id){
        return response()->json(['post'=> Lecture::find($id)]);    
    }

    function getPurchasedDetails($lecture_id){
        $post = PurchasedLecture::with('users')->where('lecture_id',$lecture_id)->get();
        return response()->json(['post'=> $post]);    
    }

    function markAsPaid($member_id){
        $user = PurchasedLecture::whereUserId($member_id)->first();
        $user->payment_status = 'paid';
        $user->save();
        return response()->json(['status'=>'success']);  
    }

}
