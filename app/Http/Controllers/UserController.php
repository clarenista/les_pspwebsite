<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Affiliation;


class UserController extends Controller
{
    public function index()
    {
        $users = User::with('affiliations')->find(1);
        return response()->json(
            [
                'status' => 'success',
                'users' => $users->toArray()
            ], 200);
    }
    public function show(Request $request, $id)
    {
        $user = User::find($id);
        return response()->json(
            [
                'status' => 'success',
                'user' => $user->toArray()
            ], 200);
    }

    public function save(Request $request){
        // dd(request()->all());
        request()->user()->update(\request()->all());
        // $user = User::find(request()->user()->id);
        // dd($request['firstName']);
        // $user->firstName = $request->firstName;
        // $user->middleName = $request->middleName;
        // $user->lastName = $request->lastName;
        // $user->birthday = $request->birthday;
        // $user->prcNumber = $request->prcNumber;
        // $user->pmaNumber = $request->pmaNumber;
        // $user->emailAddress = $request->input('emailAddress');
        // $user->homeAddress = $request->input('homeAddress');
        // $user->homeAddressCity = $request->input('homeAddressCity');
        // $user->homeAddressCountry = $request->input('homeAddressCountry');
        // $user->homeAddressZipCode = $request->input('homeAddressZipCode');
        // $user->mobileNumber = $request->input('mobileNumber');
        // $user->landlineNumber = $request->input('landlineNumber');
        // $user->faxNumber = $request->input('faxNumber');

        // $user->save();
        return response()->json(['user' => request()->user()]);
    }

    function resetPassword(Request $request){
        // $isPasswordResetKey = User::where('passwordresetkey' , $this->generatePasswordResetKey())->first();
        // if(!$isPasswordResetKey){
        //     $user = User::where('emailAddress' , $request->email)->first();
        //     if($user){
        //         $user->passwordresetkey = $this->generatePasswordResetKey();
        //         $user->save();
        //         return $this->sendPasswordResetEmail($user, $request->root());
        //         // return response()->json(['user' => $user]);
        //     }else{
        //         return response()->json(['status' => 'failed']);
        //     }
        // }

        $guzzle = new \GuzzleHttp\Client;
        $token = $guzzle->post(config('app.domain') . '/oauth/token', [
            'form_params' => [
                'grant_type' => config('app.passport.grant_type'),
                'client_id' => config('app.passport.client_id'),
                'client_secret' => config('app.passport.client_secret'),
                'scope' => '*',
            ],
        ]);
        $response = $guzzle->post(config('app.domain') . '/api/password-remind', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . json_decode((string) $token->getBody(), true)['access_token'],
            ],
            'form_params' => [
                'email' => $request->email,
                'domain' => $request->root(),
            ],
        ]);
        $result = json_decode((string) $response->getBody(), true);
        // dd($result);
        // return response()->json(['user' => $user]);
    }

    private function sendPasswordResetEmail($user, $host){
        $to_email = $user->emailAddress;
        $data = array("name" => $user->firstName, 'prkey' => $user->passwordresetkey, 'host' => $host, 'username'=>$user->email);
        Mail::send('emails.mail', $data, function($message) use ($to_email) {
            $message->to($to_email)
                    ->subject('PSP Member Portal Password Reset');
            $message->from('pspinfotech19@gmail.com','PSP Member Portal Password Reset');
        });        

        return response()->json(['status' => 'success']);
    }

    private function generatePasswordResetKey(){
        return str_random(32);
    }

    function checkprkey($prkey){
        $isPasswordResetKey = User::where('passwordresetkey' , $prkey)->first();
        if($isPasswordResetKey){
            return response()->json(['isExists' => true]);
        }
    }

    function passwordResetSave(Request $request){

        $guzzle = new \GuzzleHttp\Client;
        $token = $guzzle->post(config('app.domain') . '/oauth/token', [
            'form_params' => [
                'grant_type' => config('app.passport.grant_type'),
                'client_id' => config('app.passport.client_id'),
                'client_secret' => config('app.passport.client_secret'),
                'scope' => '*',
            ],
        ]);
        $response = $guzzle->post(config('app.domain') . '/api/reset-password', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . json_decode((string) $token->getBody(), true)['access_token'],
            ],
            'form_params' => [
                'reset_password_key' => $request->reset_password_key,
                'password' => $request->password,
            ],
        ]);
        $result = json_decode((string) $response->getBody(), true);

        // $user = User::where('passwordresetkey' , $request->prkey)->first();
        // $user->password = Hash::make($request->password);
        // $user->passwordresetkey = '';
        // $user->save();
        // return response()->json(['status' => 'success']);
    }

    function showUser(){
        return response()->json([
            'user' => request()->user(),
            'evaluations' => request()->user()->evaluations,
            'affiliations' => request()->user()->affiliations,
        ]);
    }

}
