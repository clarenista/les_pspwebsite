<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Committee;
use App\Council;

class CommitteesController extends Controller
{
    function index(){
    	return view('committees.index', ['councils' => Council::with('committees')->get()]);
    }

    function fetchAll(){
    	return response()->json(['committees' => Committee::all()]);
    }

    function saveCommittees(Request $request){
        $committee  = new Committee();
        $committee->name = $request->input('name');
        $committee->position = $request->input('position');
        $committee->row_position = $request->input('row_position');
        $committee->council_id = $request->input('council_id');
        $committee->save();
        $lastIdInserted = Committee::select('id')->orderBy('id', 'desc')->first();
        $ccouncil = Committee::find($lastIdInserted->id);
        $id = $ccouncil->id;
        if(strlen($request->image) != null){
            $imageName = $id.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/committees'), $imageName);
            $ccouncil->display_picture = '/uploads/committees/'.$id.'.'.$request->image->getClientOriginalExtension();
        }else{
            $ccouncil->display_picture = '';

        }
        $ccouncil->save();        
        return response()->json(['status'=>'success']); 
    }

    function getCommitteeDetails($id){
    	return response()->json(['post' => Committee::find($id)]);
    }

    function updateCommittees(Request $request){
        $board  = Committee::find($request->input('id'));
        $board->name = $request->input('name');
        $board->position = $request->input('position');
        $board->row_position = $request->input('row_position');
        if(strlen($request->image) != null){
	 		$imageName = $request->input('id').'.'.$request->image->getClientOriginalExtension();
	        $request->image->move(public_path('uploads/committees'), $imageName);
	        $board->display_picture = '/uploads/committees/'.$request->input('id').'.'.$request->image->getClientOriginalExtension();
        }else{
            $board->display_picture = '';

        }

        $board->save();
        return response()->json(['status'=>'success']);        
    }

    function deleteCommittee($id){
        $officer = Committee::find($id);
        $officer->delete();
        return response()->json(['status' => 'success']);
    }        
}



    // function getPatho(){
    //     return response()->json(['boards' => BoardPathology::all()]);
    // }        

    // function getPathoDetails($id){
    //     $board = BoardPathology::find($id);
    //     return response()->json(['post' => $board]);

    // }        

    // function savePatho(Request $request){
    //     $board  = new BoardPathology();
    //     $board->name = $request->input('name');
    //     $board->position = $request->input('position');
    //     $board->save();
    //     $lastIdInserted = BoardPathology::select('id')->orderBy('id', 'desc')->first();
    //     $bPatho = BoardPathology::find($lastIdInserted->id);
    //     $id = $bPatho->id;
    //     $imageName = $id."_".$request->input('position').'.'.$request->image->getClientOriginalExtension();
    //     $request->image->move(public_path('uploads/board-of-pathology'), $imageName);
    //     $bPatho->display_picture = '/uploads/board-of-pathology/'.$id."_".$request->input('position').'.'.$request->image->getClientOriginalExtension();
    //     $bPatho->save();        
    //     return response()->json(['status'=>'success']);     
    // }        

    // function updatePatho(Request $request){
    //     $board  = BoardPathology::find($request->input('id'));
    //     $board->name = $request->input('name');
    //     $board->position = $request->input('position');
    //     if(strlen($request->image) != null){
    //         $imageName = $request->input('id')."_".$request->input('position').'.'.$request->image->getClientOriginalExtension();
    //         $request->image->move(public_path('uploads/board-of-pathology'), $imageName);
    //         $board->display_picture = '/uploads/board-of-pathology/'.$request->input('id')."_".$request->input('position').'.'.$request->image->getClientOriginalExtension();
    //     }

    //     $board->save();
    //     return response()->json(['status'=>'success']);        
    // }    
    
    // function deletePatho($id){
    //     $officer = BoardPathology::find($id);
    //     $officer->delete();
    //     return response()->json(['status' => 'success']);
    // }    