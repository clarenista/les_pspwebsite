<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CertApplication;

class CertApplicationController extends Controller
{
    public function apply(){
        request()->user()->certApplications()->create([
            'type' => request()->type,
            'receipt_path' => request()->receipt_path,
        ]);
        return respond()->json(['status' => 'ok']);
    }


}
