<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{

    public function index(){
        return view('welcome');
    }

    public function show($id){
    	$member_details = DB::table('users')->where('id',$id)->first();
    	return response()->json(['member_details' => $member_details]);
    }

    public function update(Request $request){
    	$username_exists = DB::table('users')->where('username', $request->username)->exists();
    	$email_exists = DB::table('users')->where('emailAddress', $request->emailAddress)->exists();
    	if($username_exists){
    		return response()->json([
    			'status' => 'failed',
    			'field' => 'username',
    			'message' => 'Username already exists.'
    		]);
    	}

    	if($email_exists){
    		return response()->json([
    			'status' => 'failed',
    			'field' => 'emailAddress',
    			'message' => 'Email address already exists.'
    		]);
    	}    	

    	$update = DB::table('users')
    				->where('id', $request->id)
    				->update([
    					'firstName' => $request->firstName,
    					'lastName' => $request->lastName,
    					'emailAddress' => $request->emailAddress,
    					'username' => $request->username,
    				]);
		return response()->json([
			'status' => 'success',
		]);    			
    }
}
