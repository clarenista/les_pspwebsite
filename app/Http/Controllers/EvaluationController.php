<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evaluation;
use App\EvaluationItem;

class EvaluationController extends Controller
{

    public function show(){
        $eval_items = EvaluationItem::with(['user_evaluations'=> function($q){
            $q->whereUserId(request()->user()->id);
        }])->get();
        $rates= request()->user()->evaluations()->with('item')->get();

        return response()->json([
            'eval_items' => $eval_items,
            'rates' => $rates,

        ],201);
        // $
        // return EvaluationItem::all();
        // dd();
        // return request()->user()->evaluations()->with('item')->get();
    }

    public function store(){
        $radio_rate =json_decode(request()->radio_rate);
        $content =json_decode(request()->content);
        $adherence =json_decode(request()->adherence);
        request()->validate([
            'content' => 'required',
            'adherence' => 'required',
            ]);
        foreach ($radio_rate as $key => $value) {
            
            request()->user()->evaluations()->create(
                [
                    'evaluation_item_id' => $key,
                    'rate' => $value,
                    'type' => 'radio_rate',
                ]
            );
        }
        foreach ($content as $key => $value) {
            request()->user()->evaluations()->create(
                [
                    'evaluation_item_id' => $key,
                    'rate' => $value,
                    'type' => 'content',
                ]
            );
        }

        foreach ($adherence as $key => $value) {

            request()->user()->evaluations()->create(
                [
                    'evaluation_item_id' => $key,
                    'rate' => $value,
                    'type' => 'adherence',
                ]
            );
            
        }

        request()->user()->comment()->create([
            'comment' => request()->comment
        ]);

        return response()->json([
            'evaluations' => request()->user()->evaluations
        ],201);


    }
}
