<?php

namespace App\Http\Controllers;

use App\LectureQuestion;
use Illuminate\Http\Request;

class LectureQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LectureQuestion  $lectureQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(LectureQuestion $lectureQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LectureQuestion  $lectureQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(LectureQuestion $lectureQuestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LectureQuestion  $lectureQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LectureQuestion $lectureQuestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LectureQuestion  $lectureQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(LectureQuestion $lectureQuestion)
    {
        //
    }
}
