<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;

class BannerController extends Controller
{
    function get_banner(){
    	$banner_details = Banner::find(1);
    	return response()->json(['banner_details' => json_decode($banner_details)]);
    }

    function update(Request $request){
    	$banner = Banner::find(1);
    	$banner->url = $request->url;
    	$banner->save();

    }
}
