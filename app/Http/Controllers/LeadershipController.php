<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ExecutiveOfficer;
use App\BoardGovernor;
use App\BoardPathology;
use App\RegionalChapter;
use App\RegionalLeader;
use App\Region;

class LeadershipController extends Controller
{
    function executiveofficers(){
    	return view('leadership.executiveofficers', ['eos' => ExecutiveOfficer::orderBy('row_position', 'asc')->get()]);
    }

    function boardofgovernors(){
    	return view('leadership.boardofgovernors', ['bogs' => BoardGovernor::orderBy('term', 'desc')->get()]);
    }

    function boardofpathology(){
    	return view('leadership.boardofpathology', ['bops' => BoardPathology::orderBy('row_position', 'asc')->get()]);
    }

    function regionalchapters(){
        $regions = Region::with('leaders')->get();
        // return $regions;
     //    $rLeaders = DB::table('regional_leaders')
     //                // ->select('region')
     //                // ->groupBy('region')
     //                ->get();
     //    // foreach ($rLeaders as $key => $value) {
     //    //     $rLeaders[$key]->leaders = RegionalChapter::whereRegion($value->region)->get();
     //    // }
    	return view('leadership.regionalchapters', ['regions' => $regions]);
    }


    // api EOS
    function getAllExecOfficer(){
        return response()->json(['officers' => ExecutiveOfficer::orderBy('row_position', 'asc')->get()]);
    }

    function getOfficerDetails($id){
        $officer = ExecutiveOfficer::find($id);
        return response()->json(['officer' => $officer]);

    }

    function saveOfficer(Request $request){
        $officer  = new ExecutiveOfficer();
        $officer->name = $request->input('name');
        $officer->position = $request->input('position');
        $officer->row_position = $request->input('row_position');
        $officer->term = $request->input('term');
        $officer->save();
        $lastIdInserted = ExecutiveOfficer::select('id')->orderBy('id', 'desc')->first();
        $eOfficer = ExecutiveOfficer::find($lastIdInserted->id);
        $id = $eOfficer->id;        
        if(strlen($request->image) != null){
            $imageName = $id.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/executive-officers'), $imageName);
            $eOfficer->display_picture = '/uploads/executive-officers/'.$id.'.'.$request->image->getClientOriginalExtension();
        }else{
            $eOfficer->display_picture = '';

        }        
        $eOfficer->save();
        return response()->json(['status'=>'success']);     
    }

    function updateOfficer(Request $request){
        $officer  = ExecutiveOfficer::find($request->input('id'));
        $officer->name = $request->input('name');
        $officer->position = $request->input('position');
        $officer->row_position = $request->input('row_position');
        $officer->term = $request->input('term');
        if(strlen($request->image) != null){
            $imageName = $request->input('id').'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/executive-officers'), $imageName);
            $officer->display_picture = '/uploads/executive-officers/'.$request->input('id').'.'.$request->image->getClientOriginalExtension();
        }else{
            $officer->display_picture = '';

        }

        $officer->save();
        return response()->json(['status'=>'success']);        
    }

    function deleteOfficer($id){
        $officer = ExecutiveOfficer::find($id);
        $officer->delete();
        return response()->json(['status' => 'success']);
    }


    // api BOG
    function getBoards(){
        // $bog = DB::raw("SELECT * FROM board_of_governors ORDER BY CASE WHEN term LIKE 'india%' THEN 0 ELSE 1 END, country_name");
        return response()->json(['boards' => BoardGovernor::orderBy('term', 'desc')->get()]);
    }    

    function getGovDetails($id){
        $board = BoardGovernor::find($id);
        return response()->json(['post' => $board]);

    }    

    function saveGov(Request $request){
        $board  = new BoardGovernor();
        $board->name = $request->input('name');
        $board->position = $request->input('position');
        $board->term = $request->input('term');
        $board->save();
        $lastIdInserted = BoardGovernor::select('id')->orderBy('id', 'desc')->first();
        $bGov = BoardGovernor::find($lastIdInserted->id);
        $id = $bGov->id;
        if(strlen($request->image) != null){
            $imageName = $id.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/board-of-governors'), $imageName);
            $bGov->display_picture = '/uploads/board-of-governors/'.$id.'.'.$request->image->getClientOriginalExtension();
        }else{
            $bGov->display_picture = '';
        }         
        $bGov->save();
        return response()->json(['status'=>'success']);     
    }    

    function updateGov(Request $request){
        $board  = BoardGovernor::find($request->input('id'));
        $board->name = $request->input('name');
        $board->position = $request->input('position');
        $board->term = $request->input('term');
        if(strlen($request->image) != null){
            $imageName = $request->input('id').'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/board-of-governors'), $imageName);
            $board->display_picture = '/uploads/board-of-governors/'.$request->input('id').'.'.$request->image->getClientOriginalExtension();
        }else{
            $board->display_picture = '';

        }

        $board->save();
        return response()->json(['status'=>'success']);        
    }    
    
    function deleteGov($id){
        $officer = BoardGovernor::find($id);
        $officer->delete();
        return response()->json(['status' => 'success']);
    }    


    // api BOG
    function getPatho(){
        return response()->json(['boards' => BoardPathology::orderBy('row_position', 'asc')->get()]);
    }        

    function getPathoDetails($id){
        $board = BoardPathology::find($id);
        return response()->json(['post' => $board]);

    }        

    function savePatho(Request $request){
        $board  = new BoardPathology();
        $board->name = $request->input('name');
        $board->position = $request->input('position');
        $board->row_position = $request->input('row_position');
        $board->save();
        $lastIdInserted = BoardPathology::select('id')->orderBy('id', 'desc')->first();
        $bPatho = BoardPathology::find($lastIdInserted->id);
        $id = $bPatho->id;
        if(strlen($request->image) != null){
            $imageName = $id.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/board-of-pathology'), $imageName);
            $bPatho->display_picture = '/uploads/board-of-pathology/'.$id.'.'.$request->image->getClientOriginalExtension();
        }else{
            $board->display_picture = '';
        }        
        $bPatho->save();        
        return response()->json(['status'=>'success']);     
    }        

    function updatePatho(Request $request){
        $board  = BoardPathology::find($request->input('id'));
        $board->name = $request->input('name');
        $board->position = $request->input('position');
        $board->row_position = $request->input('row_position');
        if(strlen($request->image) != null){
            $imageName = $request->input('id').'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/board-of-pathology'), $imageName);
            $board->display_picture = '/uploads/board-of-pathology/'.$request->input('id').'.'.$request->image->getClientOriginalExtension();
        }else{
            $board->display_picture = '';
        }

        $board->save();
        return response()->json(['status'=>'success']);        
    }    
    
    function deletePatho($id){
        $officer = BoardPathology::find($id);
        $officer->delete();
        return response()->json(['status' => 'success']);
    }    

    function regionalLeader(){
        return response()->json(['regions' => Region::with('leaders')->get()]);
    }

    function allRegions(){
        return response()->json(['regions' => Region::all()]);

    }

    function saveRegion(Request $request){
        $region = new Region();
        $region->name = $request->name;
        $region->number = $request->number;
        $region->save();
        return response()->json(['status'=>'success']);  
    }

    function saveRegionalLeader(Request $request){
        $board  = new RegionalLeader();
        $board->name = $request->input('name');
        $board->position = $request->input('position');
        $board->region_id = $request->input('region');
        $board->row_position = $request->input('row_position');
        $board->save();
        $lastIdInserted = RegionalLeader::select('id')->orderBy('id', 'desc')->first();
        $regionalLeader = RegionalLeader::find($lastIdInserted->id);
        $id = $regionalLeader->id;
        if(strlen($request->image) != null){
            $imageName = $id.'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/regional-chapters'), $imageName);
            $regionalLeader->display_picture = '/uploads/regional-chapters/'.$id.'.'.$request->image->getClientOriginalExtension();
        }else{
            $regionalLeader->display_picture = '';

        }
        $regionalLeader->save();
        return response()->json(['status'=>'success']);     
    }

    function getRegionalLeaderDetails($id){
        $board = RegionalLeader::with('region')->find($id);
        $regions = Region::all();
        return response()->json(['post' => $board, 'regions' => $regions]);
    }

    function getRegionDetails($id){
        $board = Region::with('leaders')->find($id);
        return response()->json(['post' => $board]);
    }    

    function updateRegionalLeader(Request $request){
        $board  = RegionalLeader::find($request->input('id'));
        $board->name = $request->input('name');
        $board->position = $request->input('position');
        $board->row_position = $request->input('row_position');
        $board->region_id = $request->input('region');
        // $board->region = $request->input('region');
        if(strlen($request->image) != null){
            $imageName = $request->input('id').'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/regional-chapters'), $imageName);
            $board->display_picture = '/uploads/regional-chapters/'.$request->input('id').'.'.$request->image->getClientOriginalExtension();
        }else{
            $board->display_picture = '';

        }

        $board->save();
        return response()->json(['status'=>'success']);            
    }

    function deleteRegionalLeader($id){
        $regLeader = Region::find($id);
        $regLeader->delete();
        return response()->json(['status' => 'success']);
    }

    function deleteLeader($id){
        $leader = RegionalLeader::find($id);
        $leader->delete();
        return response()->json(['status' => 'success']);

    }

}