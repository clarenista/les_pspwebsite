<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Accreditation;

class AccreditationController extends Controller
{
    function index(){
    	$accredited = DB::table('accredited_institutions')
                 ->select('year')
                 ->groupBy('year')
                 ->orderBy('year', 'desc')
                 ->get();
		foreach ($accredited as $key => $value) {
			$accredited[$key]->institutions = Accreditation::where('year',$value->year)->get();
		}
    	return view('accreditation.index', ['institutions' => $accredited]);
    }

    function fetchAll(){
        return response()->json(['accredited' => Accreditation::orderBy('id', 'desc')->get()]);
    }

    function saveAccredited(Request $request){
        $accredited  = new Accreditation();
        $accredited->hospital_name = $request->input('hospital_name');
        $accredited->year_accredited = $request->input('year_accredited');
        $accredited->year = $request->input('year');
        $accredited->save();
        return response()->json(['status'=>'success']);     
    }

    function getAccredited($id){
        $accredited = Accreditation::find($id);
        return response()->json(['post' => $accredited]);

    }        

    function updateAccredited(Request $request){
        $accredited  = Accreditation::find($request->input('id'));
        $accredited->hospital_name = $request->input('hospital_name');
        $accredited->year_accredited = $request->input('year_accredited');
        $accredited->year = $request->input('year');
        $accredited->save();
        return response()->json(['status'=>'success']);   
    }

    function deleteAccredited($id){
        $accredited = Accreditation::find($id);
        $accredited->delete();
        return response()->json(['status' => 'success']);
    }        
}
