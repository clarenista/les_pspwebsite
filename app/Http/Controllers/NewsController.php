<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    function index(){
    	$latest_news = News::orderBy('id', 'desc')->first();
    	return view('news.index', ['latest_news' => $latest_news, 'news' => News::orderBy('news_timestamp', 'desc')->get()]);
    }

    function show($id){
    	$news_details = News::where('id', $id)->first();
    	return view('news.show', ['news_details' => $news_details]);
    }    

    function fetchAll(){
    	return response()->json([
    		'news' => News::orderBy('id', 'desc')->get()
    	]);
    }

    function saveNews(Request $request){
    	$news = new News();
    	$news->news_title = $request->input('title');
    	$news->news_content = $request->input('content');
    	$news->save();

    	return response()->json([
    		'status' => 'success'
    	]);
    }

    function showNewsDetails($id){
    	return response()->json([
    		'post' => News::find($id)
    	]);
    }

    function updateNews(Request $request){
    	$news = News::find($request->input('id'));
    	$news->news_title = $request->input('title');
    	$news->news_content = $request->input('content');
    	$news->save();

    	return response()->json([
    		'status' => 'success'
    	]);
    }

    function deleteNews($id){
		$news = News::find($id);
    	$news->delete();

    	return response()->json([
    		'status' => 'success'
    	]);    	
    }
}
