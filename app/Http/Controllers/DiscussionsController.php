<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Discussion;
use App\DiscussionComment;

class DiscussionsController extends Controller
{
	private function right_pane(){
		return Discussion::orderBy('id', 'desc')->get()->take(2);
	}

    function index(){
    	$main_discussion = Discussion::orderBy('id', 'asc')->get();
    	return view('discussions.index', ['discussions' => $main_discussion, 'right_pane' => $this->right_pane()]);
    }

    function show($id){
    	$discussion_details = Discussion::with('comments')->where('id', $id)->first();
    	return view('discussions.show', ['discussion_details' => $discussion_details, 'right_pane' => $this->right_pane()]);
    }


    function fetchAll(){
        return response()->json([
            'discussions' => Discussion::orderBy('id', 'desc')->get()
        ]);
    }

    function saveDiscussion(Request $request){
        $discussion = new Discussion();
        $discussion->discussion_title = $request->input('title');
        $discussion->discussion_content = $request->input('content');
        $discussion->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    function showDiscussionDetails($id){
        return response()->json([
            'post' => Discussion::find($id)
        ]);
    }

    function updateDiscussion(Request $request){
        $discussion = Discussion::find($request->input('id'));
        $discussion->discussion_title = $request->input('title');
        $discussion->discussion_content = $request->input('content');
        $discussion->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    function deleteDiscussion($id){
        $discussion = Discussion::find($id);
        $discussion->delete();

        return response()->json([
            'status' => 'success'
        ]);     
    }    

    function saveComment(Request $request, $id){
        $comment = new DiscussionComment();
        $comment->discussion_id = $id;
        $comment->comment = $request->comment;
        $comment->name = $request->name;
        $comment->email = $request->email;
        // $comment->save();

        return redirect('/discussions/'.$id);
    }
}
