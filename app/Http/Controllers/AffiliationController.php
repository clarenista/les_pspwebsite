<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Affiliation;

class AffiliationController extends Controller
{
    function destroy($affiliation_id){
    	$affiliation = Affiliation::find($affiliation_id);
    	$affiliation->delete();
    	return response()->json(['status' => 'success']);
    }

    function save(Request $request){
		$affiliation = request()->user()->affiliations()->create(\request()->all());
    	return response()->json(['affiliation' => $affiliation]);
    }

    function update(Request $request){
    	$affiliation = Affiliation::find($request->input('affiliation_id'));
    	$affiliation->hospital_name = $request->input('hospital_name');
    	$affiliation->position = $request->input('position');
    	$affiliation->hospital_address = $request->input('hospital_address');
    	$affiliation->save();
    	return response()->json(['affiliation' => $affiliation]);
    }
}
