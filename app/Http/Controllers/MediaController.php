<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MediaController extends Controller
{
    function index(){
    	$path = './uploads/media';
	    $media = array_slice(scandir($path), 2);
    	return response()->json(['media' => $media]);

    }

    function save(Request $req){
        $imageName = $req->image->getClientOriginalName();
        $req->image->move(public_path('uploads/media'), $imageName);
        return response()->json(['status'=>'success']);      	
    }
}
