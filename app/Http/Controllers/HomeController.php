<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Discussion;
use App\News;
use App\Banner;
use App\Video;

class HomeController extends Controller
{
    function index(){
    	$latest_discussion = Discussion::orderBy('id', 'desc')->first();
    	$latest_news = News::orderBy('id', 'desc')->get()->take(3);
    	$promoting_banner = Banner::first();
    	$promoting_video = Video::first();
    	return view('home.index', [
    								'latest_discussion' => $latest_discussion, 
    								'news' => $latest_news,
    								'promoting_banner' => $promoting_banner,
    								'promoting_video' => $promoting_video,
    								
    							  ]);
    }
}
