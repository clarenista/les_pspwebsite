<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $v = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password'  => 'required|min:3|confirmed',
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }
        $user = new User;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json(['status' => 'success'], 200);
    }
    public function login(Request $request)
    {
        // $credentials = $request->only('email', 'password');
        // $user = User::with('booth')->where('email', $request->email)->first();
        // if ($user) {
        //     if (Hash::check($request->password, $user->password)) {
        //         if (!$user->api_token) {
        //             $user->update(['api_token' => hash('sha256', Str::random(80))]);
        //         }
        //         Auth::login($user);
        //         return response()->json([
        //             'status' => 'ok',
        //             'user' => $user,
        //             'access_token' => $user->api_token,
        //         ]);
        //     }
        // }
        $guzzle = new \GuzzleHttp\Client;
        $token = $guzzle->post(config('app.domain') . '/oauth/token', [
            'form_params' => [
                'grant_type' => config('app.passport.grant_type'),
                'client_id' => config('app.passport.client_id'),
                'client_secret' => config('app.passport.client_secret'),
                'scope' => '*',
            ],
        ]);
        $response = $guzzle->post(config('app.domain') . '/api/user', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . json_decode((string) $token->getBody(), true)['access_token'],
            ],
            'form_params' => [
                'email' => $request->email,
                'password' => $request->password,
            ],
        ]);
        $result = json_decode((string) $response->getBody(), true);
        if ($result) {
            if ($result['status'] == "failed") {
                $user = User::with('affiliations')->where('username', $request->email)->first();
                if($user){
                    if (Hash::check($request->password, $user->password)) {
                        // The passwords match...
                        return response()->json([
                            'status' => 'ok',
                            'user' => $user,
                            'access_token' => $user->api_token,
                            'evaluations' => $user->evaluations,
                        ]);
                    }
                }
                return response()->json([
                    'status' => 'failed',
                    'message' => 'Error, please try again.',
                ]);
                $message = isset($result['message']) ? $result['message'] : "Payment not verified.";
                return response()->json([
                    'status' => 'failed',
                    'message' => $message,
                ]);
            }
            $user = User::with('affiliations')->firstOrCreate([
                'registrant_id' => $result['user']['id'],
            ], [
                'registrant_id' => $result['user']['id'],
                'api_token' => hash('sha256', Str::random(80)),
                'name' => $result['user']['first_name'] . " " . $result['user']['last_name'],
                'firstName' => $result['user']['first_name'],
                'lastName' => $result['user']['last_name'],
                'mobileNumber' => $result['user']['contact'],
                'username' => $result['user']['username'],
                'password' => $result['user']['password'],
                'email' => $result['user']['email'],
                // 'login_code' => $this->getPassword2($result['user']['first_name']),
            ]);
            return response()->json([
                'status' => 'ok',
                'user' => $user,
                'access_token' => $user->api_token,
                'evaluations' => $user->evaluations,
            ]);
        }
        return response()->json([
            'status' => 'failed',
            'message' => 'Error, please try again.',
        ]);
        // if ($token = $this->guard()->attempt($credentials)) {
        //     return response()->json(['status' => 'success'], 200)->header('Authorization', $token);
        // }
        // return response()->json(['error' => 'login_error'], 401);
    }
    public function logout()
    {
        $this->guard()->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }
    public function user(Request $request)
    {
        $user = User::with('affiliations')->find(Auth::user()->id);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }
    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }
        return response()->json(['error' => 'refresh_token_error'], 401);
    }
    private function guard()
    {
        return Auth::guard();
    }
}
