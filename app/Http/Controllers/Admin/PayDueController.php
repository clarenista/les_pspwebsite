<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Paydue;

class PayDueController extends Controller
{
    public function index($id){
        $user = User::with('dues')->find($id);
        return view('admin.paydues.index', ['user' => $user]);
    }
    
    public function approve($id,$pd_id){
        $due = Paydue::find($pd_id);

        $due->update([
            'payment_status' => 'paid'
        ]);
        return redirect()->back()->with('message','Operation Successful !');

    }
    public function approve_revert($id,$pd_id){
        $due = Paydue::find($pd_id);

        $due->update([
            'payment_status' => 'pending'
        ]);
        return redirect()->back()->with('message','Operation Successful !');

    }
}
