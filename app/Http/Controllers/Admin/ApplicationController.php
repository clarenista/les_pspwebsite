<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class ApplicationController extends Controller
{
    function index(){
        // $users = User::with('certApplications')->find(1);
        $users = User::has('certApplications')->with('certApplications')->get();
        // dd($users->certApplications[0]);
        return view('admin.applications.index', compact('users'));
    }
}
