<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lecture;
use App\LectureViewer;
use Illuminate\Support\Facades\DB;

class LectureController extends Controller
{
    public function index(){

        return view('admin.lectures.index', ['lectures' => Lecture::all()]);
    }
    
    public function create(){
        $lecture = new Lecture;
        return view('admin.lectures.create',  \compact('lecture'));

    }
    
    public function store(Request $request)
    {
        
        // dd($request->all());
        DB::transaction(function () use ($request) {
            
            $lecture = Lecture::create(
                $request->validate([
                    'title' => 'required',
                    ])
                );
            // $lecture->type = 'lecture';

            $this->uploadFile($lecture, 'file_path');
            
        });

        return \redirect()->route('lectures.index')
            ->with('success', 'You have successfully added a Booth.');
    }
        
    public function edit(Lecture $lecture){
        $lecture->load('active_viewers');
        // $sponsors = User::doesntHave('booth')->role('sponsor')->get();

        // return view("cms.booth.form", \compact('booth', 'sponsors'));
        return view('admin.lectures.create',  \compact('lecture'));

    }
    public function update(Request $request, Lecture $lecture)
    {
        DB::transaction(function () use ($lecture) {

            $lecture->update(
                \request()->validate([
                    'title' => 'required|string',
                ])
            );
            $this->uploadFile($lecture, 'file_path');
            

            
        });
        return \redirect()->route('admin.lectures.index')->with('success', 'You have successfully updated a Lecture.');
    }

    public function updateIsWatching(Request $request, $id){
        $viewer = LectureViewer::find($id);
        $viewer->update(['is_watching' => 0]);
        return \redirect()->back();
    }
}
