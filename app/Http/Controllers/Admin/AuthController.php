<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function adminlogin(){
        // return bcrypt('123');
        return view('admin.login');
    }

    public function adminlogingin(Request $request){
        // $panel = \App\User::find(725);
        // if (Hash::check('123', $panel->password)) {
        //     // The passwords match...
        //     return 'meron';
        // }
        
        // dd($request->all());
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            if(Auth::user()->role == 0){

                return redirect('/admin/lectures');
            }else{
                
                return back()->withErrors([
                    'email' => 'Not Authorized',
                ]);
            }
        }
        
        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
            ]);
    }
    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }
}
