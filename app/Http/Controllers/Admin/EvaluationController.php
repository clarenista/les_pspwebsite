<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Evaluation;
use App\EvaluationItem;
use App\User;
use App\UserEvalComment;

class EvaluationController extends Controller
{
    public function index(){
        $eval_items = EvaluationItem::with('user_evaluations')->with('user_content_eval')->with('user_adherence_eval')->get();
        $hasEvalCount = count(User::withCount('evaluations')->having('evaluations_count', '>', 0)->get());
        $comments = UserEvalComment::all();
        foreach ($eval_items as $item) {
            # code...
            if($item->day != '1'){
                $item['content'] = round(($item->user_content_eval->sum('rate') / $hasEvalCount), 2);
                $item['adherence'] = round(($item->user_adherence_eval->sum('rate') / $hasEvalCount), 2);

                // foreach ($item->user_evaluations as $eval) {
                //     if($eval->type =='content'){
    
                //         $item['content'] = $eval->sum('rate');
                //     }else{
                //         $item['adherence'] = round(($eval->sum('rate') / $hasEvalCount), 2);
                        
                //     }
                //     # code...
                // }
            }else{

                $item['average'] = round(($item->user_evaluations->sum('rate') / $hasEvalCount), 2);
            }
            // dd($item->user_evaluations->sum('rate'));
            // $eval_items->user_evaluations[$key]->sum  = $eval_items[$key]->user_evaluations->sum('rate');
        }
        // dd($eval_items);
        return view('admin.evaluations.index', ['eval_items' => $eval_items, 'hasEvalCount' =>  $hasEvalCount, 'comments' => $comments]);
    }
}
