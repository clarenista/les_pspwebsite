<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\User;
use App\UserEvalComment;
use App\EvaluationItem;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{
    public function index(Request $request){
        $hasEvalCount = count(User::withCount('evaluations')->having('evaluations_count', '>', 0)->get());
        // var_dump(User::withCount('evaluations')->having('evaluations_count', '>', 0)->get());
        if($request->hasEval){
            // var_dump();

            return view('admin.users.index', ['users' => User::withCount('evaluations')->having('evaluations_count', '>', 0)->get()]);
        }
        $user =  User::withCount('evaluations')->whereRole(1)->get();

        return view('admin.users.index', ['users' => $user, 'hasEvalCount' => $hasEvalCount]);
    }

    public function show($id){
        return view('admin.users.show', ['user' => User::with('affiliations')->find($id)]);
        
    }
    public function update(Request $request, $id){
        User::find($id)->update(['year_accepted' => $request->year_accepted]);
        return redirect()->back()->with('message','Operation Successful !');
    }

    public function showEval($id){
        $eval_items = EvaluationItem::with(['user_evaluations'=> function($q) use($id){
            $q->whereUserId($id);
        }])->get();
        $eval_items['comment'] = UserEvalComment::where('user_id', $id)->first();
        // dd($eval_items);
        return view('admin.users.showEval', ['eval_items' => $eval_items]);
        
    }

    function createAdmin(){
        $user = new User;
        return view('admin.users.createAdmin', \compact('user'));
    }

    function storeAdmin(Request $request){

        
        
        // dd($request->all());
        DB::transaction(function () use ($request) {
            
            $user = User::create([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'email' => $request->email,
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'api_token' => hash('sha256', Str::random(80)),
                'role' => $request->type,
            ],
                $request->validate([
                    'username' => 'required',
                    'password' => 'required',
                    ])
                );
            // $lecture->type = 'lecture';

            
        });

        return \redirect()->route('createAdmin')
            ->with('success', 'You have successfully added a Booth.');

    }

    public function export(){
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
