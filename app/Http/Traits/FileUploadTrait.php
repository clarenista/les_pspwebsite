<?php

namespace App\Http\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

trait FileUploadTrait
{

    public function uploadFile(Model $model, $name = 'file', $field = 'file_path')
    {

        // dd($model);
        if (request()->hasFile($name)) {
            $file = request()->file($name);
            $file_name = time() . "-" . $file->getClientOriginalName();
            $file->storeAs("{$model->title}/", $file_name);
            $model->update([
                $field => Storage::url("{$model->title}/{$file_name}"),
            ]);
        }
    }
}
