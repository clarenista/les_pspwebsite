<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegionalChapter extends Model
{
    protected $table = 'regional_leaders';
}
