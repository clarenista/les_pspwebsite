<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEvalComment extends Model
{
    //
    protected $fillable = ['user_id', 'comment'];
}
