<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use App\Affiliation;

class User extends Authenticatable
{
    use Notifiable , HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'registrant_id',
        'api_token',
        'name',
        'firstName',
        'lastName',
        'mobileNumber',
        'username',
        'password',
        'email',

        'middleName',
        'lastName',
        'birthday',
        'prcNumber',
        'pmaNumber',
        'emailAddress',
        'homeAddress',
        'homeAddressCity',
        'homeAddressCountry',
        'mobileNumber', 
        'landlineNumber',
        'faxNumber', 
        'homeAddressZipCode',
        'philhealth_accreditation_no',
        'residency_training_institution',
        'classification',
        'status',
        'year_accepted',
        'specialty',
        'chapter_membership',
        'area_of_practice',
        'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    public function getJWTCustomClaims()
    {
        return [];
    }    

    function affiliations(){
        return $this->hasMany('App\Affiliation', 'user_id', 'id');
    }

    function evaluations(){
        return $this->hasMany('App\Evaluation', 'user_id', 'id');
    }
    
    function certApplications(){
        return $this->hasMany('App\CertApplication', 'user_id', 'id');
        
    }
    
    function comment(){
        return $this->hasOne('App\UserEvalComment', 'user_id', 'id');

    }
    function dues(){
        return $this->hasMany('App\Paydue', 'user_id', 'id');
        
    }
    
    function lecture_views(){
        return $this->hasMany('App\LectureViewer', 'user_id', 'id');

    }
}
