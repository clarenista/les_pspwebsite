<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Region;

class RegionalLeader extends Model
{
    protected $table = 'r_leaders';

    function region(){
    	return $this->hasOne(Region::class, 'id', 'region_id');
    }
}
