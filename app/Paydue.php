<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paydue extends Model
{
    protected $table = 'pay_due_details';

    protected $fillable = ['payment_status'];
}
