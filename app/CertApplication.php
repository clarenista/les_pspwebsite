<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class CertApplication extends Model
{
    protected $fillable = ['user_id', 'type', 'receipt_path'];
    public function user(){
        return $this->belongsTo('App\User', 'user_id' , 'id');
    }
}
