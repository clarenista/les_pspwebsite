<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LectureQuestion;

class Lecture extends Model
{

    protected $fillable = ['title', 'file_path'];

    function questions(){
    	return $this->hasMany(LectureQuestion::class, 'lecture_id', 'id')->inRandomOrder();
    }

    public function viewers(){
        return $this->hasMany('App\LectureViewer', 'lecture_id', 'id');
    }

    public function active_viewers(){
        return $this->hasMany('App\LectureViewer', 'lecture_id', 'id')->with('user')->where('is_watching', 1);
    }
}
