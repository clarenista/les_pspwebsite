<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accreditation extends Model
{
   protected $table = 'accredited_institutions';
}
