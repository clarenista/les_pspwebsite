<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DiscussionComment;

class Discussion extends Model
{
    protected $table = 'discussions';

    function comments(){
    	return $this->hasMany('App\DiscussionComment');
    }
}
