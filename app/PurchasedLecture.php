<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lecture;
use App\User;

class PurchasedLecture extends Model
{
    protected $table = 'lecture_purchase_details';

    function lecture_details(){
    	return $this->hasOne(Lecture::class, 'id', 'lecture_id');
    }

    function users(){
    	return $this->hasMany(User::class, 'id', 'user_id')->select(['id','firstName', 'lastName']);
    }

}
