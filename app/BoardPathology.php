<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardPathology extends Model
{
    protected $table = 'board_of_pathology';
}
