@extends('app')

@section('content')

<div class="py-5">
	<div class="container ">
		<div class="row justify-content-center">
			
				<div class="col-lg-8 col-sm-12">
					<h1 class="text-success pb-5">List of Accredited Institutions</h1>
					<table class="table table-hover table-inverse">
						@foreach($institutions as $institution)
						<?php $count = 1; ?>
							<thead>
								<th class="bg-sec">
									<td colspan="3" class="bg-success"><h3 class="text-light">{{$institution->year}}</h3></td>
								</th>
							</thead>
							<tbody >
								@foreach($institution->institutions as $item)	
									<tr>
										<td>{{$count++}}</td>
										<td>{{$item->hospital_name}}</td>
										<td nowrap="">{{$item->year_accredited}}</td>
									</tr>
								@endforeach
							</tbody>
						@endforeach
					</table>
					
				</div>
		</div>
	</div>
</div>

<div class=" bg-light">
	<div class="container py-5">
		<h1 class="text-success">Requirements for Residency Training Program</h1>

		<h5 class="text-justify">A department of pathology and clinical laboratory may be accredited for a combined residency training in anatomic and clinical pathology. After the inspection and approval, the accreditation will be for a period of three (3) years after which a reapplication should be done.</h5>
		<hr>		
		<p><strong>General requirements for accreditation for a combined residency training program in anatomic and clinical pathology:</strong></p>
		<ol>
			<li>The department of pathology may be part of a medical school accredited by the Commission on Higher Education (CHED). The clinical laboratory must be part of a hospital licensed by the Department of Health, Health Facilities and Services Regulatory Bureau (DOH, HFSRB).</li>
			<li>The laboratory shall be a duly registered tertiary laboratory licensed to operate by the DOH, HFSRBs as mandated by the Clinical Laboratory Law (R.A. 4688).</li>
			<li>The head of the laboratory shall be a fellow of the PSP in clinical pathology and/or anatomic pathology. There shall be a minimum of two associate pathologist/s who are at least diplomate/s of the PSP in Clinical Pathology and/or Anatomic Pathology. In instances where there are no fellows, a diplomate of the PSP can be the head and the department can still be accredited on a temporary basis up to a maximum of two years.</li>
			<li>The technologists-in-charge of the various sections of the laboratory shall be registered with the Board of Medical Technology.</li>
			<li>There shall be adequate physical plant and work space commensurate with the volume of work being done and the number of personnel and trainees.</li>
			<li>There shall be sufficient and appropriate equipment, instruments, glassware, reagents and supplies commensurate with the work being done.</li>
			<li>There shall be a medical library containing references in anatomic and clinical pathology in the premises of the department or within the hospital or medical school.</li>
			<li>The formal training program shall adhere to the PSP Outcome-Based Training Program in Anatomic Pathology and Clinical Pathology and should include the following:</li>

			<ul style="list-style-type:circle;">
				<li>General objectives of the program</li>
				<li>Specific objectives of the training in various sections of the department</li>
				<li>Schedule of rotations</li>
				<li>Schedule of departmental conferences</li>
				<li>Supervision by consultant staff</li>
				<li>Duties and responsibilities of residents</li>
				<li>Evaluation scheme</li>
				<li>Research requirements</li>
			</ul>
		</ol>
		<p><strong>The following are required for approval of a four-year combined residency training program in anatomic pathology and clinical pathology:</strong></p>
		<ol>
			<li>The section of anatomic pathology shall have the following services:</li>
			<ul style="list-style-type:circle;">
				<li>Surgical pathology</li>
				<li>Cytology</li>
				<li>Autopsy</li>
				<li>Frozen section</li>
			</ul>
			<li>There shall be a hospital policy requiring mandatory submissions of all surgical specimens to the laboratory.</li>
			<li>There shall be adequate facilities for filing and rapid retrieval of histologic slides, photographs, protocols, reports and specimens.</li>
			<li>The following minimum volume of work are required for one (1) resident:</li>
			<ul style="list-style-type:circle;">
				<li>Surgical pathology…………………………….600 specimens/annum</li>
				<li>Cytology………………………………………..300 specimens/annum</li>
				<li>Autopsy…………………………………………. 10 cases/4 yrs</li>
				<li>Frozen section…………………………………..10 cases/annum</li>
			</ul>
			<li>The section of clinical pathology shall have the following services:</li>
			<ul style="list-style-type:circle;">
				<li>Hematology</li>
				<li>Clinical microscopy</li>
				<li>Blood bank</li>
				<li>Microbiology</li>
				<li>Immunology</li>
				<li>Clinical chemistry</li>
			</ul>
			<li>The minimum volume of work required is 20,000 tests/year per resident and should include sufficient variety of tests from the services enumerated in 11.2.5.</li>
			<li>The number of tests in excess of the minimum requirements shall be considered in determining if the laboratory may have more than the stated number of residents.</li>
			<li>Institutions that cannot comply with the minimum volume of work required in the combined program may still be accredited provided arrangements are made for its residents to rotate in another institution where the pathologist is at least a diplomate of the PSP.</li>
			<li>There should be a ratio of one consultant in anatomic pathology or clinical pathology for every three residents. If the consultant is a diplomate in anatomic and clinical pathology, the consultant shall be considered as one in each section.</li>
		</ol>
		<p><strong>The following fees are applicable:</strong></p>
			<ol>
				<li>Application fee: PHP 25,000.00</li>
				<li>Renewal fee: PHP 15,000.00</li>
			</ol>
		<p><strong>Deadline for submission of documents for accreditation or renewal shall be three (3) months before expiration of accreditation.</strong></p>
		<p class=""><i><u>Downloadable Documents</u></i></p>
		<ul>
			<li>
				<u>
					<a href="{{url('downloadables/Application-Form.doc')}}">APPLICATION FOR ACCREDITATION</a>
				</u>
			</li>
			<li>
				<u>
					<a href="{{url('downloadables/Survey-Form.doc')}}">SURVEY FORM FOR APPLICATION FOR ACCREDITATION</a>
				</u>
			</li>
			<li>
				<u>
					<a href="{{url('downloadables/CHECKLIST-OF-ACCREDITATION.xls')}}">CHECKLIST OF ACCREDITATION</a>
				</u>
			</li>
		</ul>
	</div>
</div>

@endsection