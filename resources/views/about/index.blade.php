@extends('app')


@section('content')

<!-- History Row -->
<div class="bg-light">
	<div class="h-100 w-100 py-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col">
					<h1 class="text-success">History</h1>
					<hr>
					<p class="text-dark text-justify">The organizational meeting was held at the Aristocrat Restaurant on Roxas Boulevard on the night of April 11, 1950. The first set of officers was elected namely: President, Dr. Liborio Gomez; Vice-President, Dr. Juan Z. Sta. Cruz; Secreatry-Treasurer, Dr. Benjamin A, Barrera; Governors: Drs. Onofre Garcia, Walfredo De Leon, Manuel D. Penas, and Benjamin A. Barrera. </p>

					<p class="text-dark text-justify">The PSP Board of Pathology was created in the year 1966-1967 to administer the examinations and grants certificates of qualification for the practice of pathology after satisfactory demonstration as Regular Member and eventual evaluation of Fellow in the Society. </p>		
																
				</div>

				<div class="col d-none d-lg-block">
				  	<div class="p-0 justify-content-center">
		    	        <div class="jumbotron jumbotron-fluid p-0">
		    	        	<img src="/images/history.jpg" class="h-100 w-100 rounded shadow" alt="">
		    			</div>
				  	</div>				
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col">
					<p class="text-dark text-justify">On June 18, 1966 the Clinical Laboratory Law (RA 4688) was passed by the 6th Congress of the Philippines with valuable contributions extended by the Society in defining the role of the profession and scope of practice of pathology in the country. Subsequently the implementing rules governing the establishment, registration, operation and maintenance of clinical laboratories were approved and signed by then Secretary of Health Paulino J. Garcia. The Society was also admitted as a specialty affiliate of the Philippine Medical Association. </p>		
					
					<p class="text-dark text-justify">In the following years, the need for improving quality patient care with assurance of quality laboratory output became the specific thrust of the Society with the cooperation of the Bureau of Research and Laboratories.  </p>									
					<p class="text-dark text-justify">The Society undertook series of seminars, workshops and symposia in collaboration with some national specialty, international organizations such as the World Health Organization and government agencies particularly the Ministry of Health. These activities are now the mainstay of the current thrust to upgrade both manpower and institutional developments of the country. </p>	

					<p class="text-dark text-justify">The ground breaking and cornerstone laying on the side of the future PSP builing was held on February 20, 1986. The PSP also became member of the Asia Pacific Association of Society of Pathologists. </p>

					<p class="text-dark text-justify">The turn of the century was characterized by rapid advances in Medicine, computer technology and communication. The new millennium poses new challenges. As the Society celebrates its 50th year of existence, its commitment to excellence in health service, training and research remains unabated bringing the practice of pathology to a renewed and grater height. It could be stated that the practice of pathology in the Philippines is at par with those in other countries and the Filipino pathologists is an important partner of other doctors in the management of patients.</p>				
				</div>

				
			</div>
		</div>
	</div>
</div>

<!-- Mission Vision -->
<div class="py-5">
	<div class="container">
		<div class="row justify-content-center">
			
			<!-- Mission -->
			<div class="col-lg-6 col-sm-12">
				<h1 class="text-success">Mission</h1>
				<hr>
				<p class="text-justify">
					To foster solidarity through proactive participation in society activities and advocacies enhanced communication and adherence to professional and ethical standards.
				</p>
				<p class="text-justify">
					To train highly competent pathologists by providing continuing education and training to our members. 
				</p>
				<p class="text-justify">
					To lead the way towards the advancement in the field of Pathology and Laboratory Medicine through active participation in local and global collaborative research. 
				</p>
				<p class="text-justify">
					To promote the role of socially responsible Pathologists in the healthcare system by coordinating with policy makers, stakeholders, other societies and healthcare partners in service, training and research in the local and international arena in support of the national health agenda.
				</p>
			</div>
			
			<!-- Vision -->
			<div class="col-lg-6 col-sm-12">
				<h1 class="text-success">Vision</h1>
				<hr>
				<p class="text-justify">
					The Philippine Society of Pathologists will be a unified and cohesive force composed of highly competent, globally recognized professionals collaborating with local and international health partners, including the academe and other societies working towards the advancement of the profession of Pathology and Laboratory Medicine. 
				</p>
				<p class="text-justify">
					We will be at the forefront of patient care while adhering to the highest standards in service, training and research.
				</p>
			</div>		
		</div>
	</div>
</div>

<!-- Code of Ethics -->
<div class="bg-light">
	<div class="h-100 w-100 py-5">
		<div class="container">
			<div class="row justify-content-center">

				<div class="col-4 d-none d-lg-block">
				  	<div class="justify-content-center">
		    	        <div class="jumbotron jumbotron-fluid p-0">
		    	        	<img src="/images/checkbox.png" class="w-75" alt="">
		    			</div>
				  	</div>				
				</div>				
				<div class="col-lg-8 col-sm-12">
					<h1 class="text-success">Code of Ethics</h1>
					<hr>
					<p class="text-dark text-justify">I DO SOLEMNLY SWEAR to adhere to the “Principles of Medical Ethics” of the Philippine Medical Association and to the Code of Ethics of the Philippine Society of Pathologists. Towards this end, I shall devote my services to the greatest benefit of the sick and injured and to ensure the fullest measure of cooperation with my colleagues. I further commit myself to adhere to the following canons of professional ethics:</p>

					<p class="text-dark text-justify">The PSP Board of Pathology was created in the year 1966-1967 to administer the examinations and grants certificates of qualification for the practice of pathology after satisfactory demonstration as Regular Member and eventual evaluation of Fellow in the Society. </p>		
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col">
					<p class="text-dark text-justify">
						<span><strong class="bg-sec">I SHALL NOT SOLICIT,</strong></span> directly or indirectly, or any manner whatsoever, or knowingly permit others to solicit in my behalf, nor shall I accept, a position which is occupied or about to vacated without first consulting with the incumbent or outgoing pathologist; 						
					</p>

					<p class="text-dark text-justify">
						<span><strong class="bg-sec">I SHALL NOT ISSUE</strong></span> a report on preparations or material from another pathologist, or another laboratory or from other institutions which another pathologist serves, without first making every reasonable effort to inform that first pathologist of the request for second examination or opinion and before I decide to issue a result of a second opinion; 		
					</p>	
					
					<p class="text-dark text-justify">
						<span><strong class="bg-sec">I SHALL NOT DIVIDE,</strong></span> either directly or by means of any subterfuge, fees for laboratory services with referring physicians; 	
					</p>		
					<p class="text-dark text-justify">
						<span><strong class="bg-sec">I SHALL NOT COMPETE,</strong></span> for laboratory services on the basis of fees; 
					</p>	
					<p class="text-dark text-justify">
						<span><strong class="bg-sec">I SHALL NOT ISSUE</strong></span> reports to patients except when requested to do so by the patient’s attending physician; 
					</p>					
					<p class="text-dark text-justify">
						<span><strong class="bg-sec">I SHALL NOT PARTICIPATE,</strong></span> directly or by means of any subterfuge, in an arrangement or scheme whereby an individual not duly licensed to practice medicine and not certified nor a member of the Society, is allowed to operate a laboratory, clinical or otherwise; 
					</p>	

					<p class="text-dark text-justify">
						<span><strong class="bg-sec">I SHALL NOT ACCEPT</strong></span> a position in any hospital, institution or other medical organization which does not protect the welfare and interest of the pathologist; 
					</p>																								

					<p class="text-dark text-justify">
						<span><strong class="bg-sec">I SHALL NOT ALLOW</strong></span> myself to be a willing tool for political purposes nor for the personal interest of others to the prejudice of another pathologist or colleagues in the allied profession; 
					</p>			
					<p class="text-dark text-justify">
						<span><strong class="bg-sec">I SHALL NEVER SPEAK</strong></span> ill of the society or any of my colleagues, nor shall I bring forth any issue before any other forum, administrative or judicial, without first having exhausted all avenues of negotiation or settlement within the society; 
					</p>
					<p class="text-dark text-justify">
						<span><strong class="bg-sec">I SHALL ALWAYS SHOW RESPECT</strong></span> to the officers of the society as well as to its elders and to the principles that they represent. Towards this end, I shall always exert effort to support the programs and activities of the society and, that further, I shall exert every effort to contribute to the success of its endeavor.
					</p>													
				</div>
			</div>
		</div>
	</div>
</div>

<!-- PURPOSE & OBJECTIVES -->
<div class="py-5">
	<div class="container">
		<div class="row justify-content-center">
			
			<div class="col">
				<h1 class="text-success">Purpose & Objectives</h1>
				<hr>
				<p>
					PSP in one of the eight medical specialty societies recognized by the Philippine Medical Association. It is presently compose of 600 members with its own office building at 114 Malakas Streeet, Diliman, Quezon City. 
				</p>
				<p class="text-justify">
					The PSP was established with the following major purpose and objectives. 
				</p>

				<div class="col">
					<ul class="timeline">
						<li>
							<p class="text-justify">To promote the practice of pathology by stimulating better appreciation and proper evaluation of laboratory services among physicians, hospitals and the public in general. </p>
						</li>
						<li>
							<p class="text-justify">To prescribe standards in the laboratory procedures, techniques and management.</p>
						</li>
						<li>
							<p class="text-justify">To foster and maintain the highest standards in education, research and practice of pathology. </p>
						</li>
						<li>
							<p class="text-justify">To unify the pathologists in the Philippine into one cohesive and productive organization.</p>
						</li>
						<li>
							<p class="text-justify">To promote the welfare of the members of the society.</p>
						</li>
						<li>
							<p class="text-justify">To determine the competence of those desiring to practice this specialty and its subspecialties.</p>
						</li>																																	
						<li>
							<p class="text-justify">To established cordial relationships among the members of this society and other national and international scientific societies.</p>
						</li>									
					</ul>				
				</div>
			</div>
		</div>
	</div>
</div>

<!-- RIGHTS & OBLIGATIONS -->
<div class="bg-light">
	<div class="h-100 w-100 py-5">
		<div class="container">
			<h1 class="text-success">Rights & Obligations</h1>
			<hr>
			
			<h5>1. A member shall be deemed in good standing or an active member when: </h5>
			<ul>
				<li>He has paid his dues and assessments for the current and previous years; </li>
				<li>He has attended the business meeting unless excused by the Board of Governors for justifiable reasons submitted in a written explanation;  </li>
				<li>He has earned at least 50 points in the preceding year as provided for in Section 1.5.2.3; and ; </li>
				<li>He has complied with other obligations that may be imposed by the society through its duly constituted officers or through the Board of Governors ; </li>
			</ul>

			<h5>2. An active member shall enjoy the following rights and obligations: </h5>
			<ul>
				<li>Shall participate in the activities and functions of the society. </li>
				<li>Diplomates and fellows have voice and vote in the deliberations of the society. </li>
				<li>Diplomates and fellows may be appointed to committees and have other prerogatives of a member of the society. </li>
				<li>Only fellows are eligible for office except that two (2) out of eleven (11) members of the Board of Governors other than the Executive Officers may be diplomates.  </li>
				<li>Diplomates and fellows are qualified to head a clinical laboratory, blood bank or sections thereof pursuant to Rep. Act. No. 4688, its implementing rules and regulations, DOH circulars or administrative orders, and such other government issuances implementing the Clinical Laboratory Law.   </li>
				<li>Corresponding and junior members may be allowed to attend society functions as appropriate and participate actively in scientific sessions; they may have voice but not vote in the deliberation of the society. They cannot be head of any clinical laboratory or sections thereof pursuant to existing laws (Rep Act No 4688).   </li>
				<li>Emeritus and honorary fellows shall have voice but not vote in all deliberations of the society.  </li>
				<li>Active members may wear the official pin.  </li>
				<li>Fellows or diplomates may affix the letters “FPSP” or “DPSP” as the case may be (Fellow of the Philippine Society of Pathologists or Diplomate of the Philippine Society of Pathologists, respectively) after their names to indicate their membership status in the Society.  </li>
				<li>When a member fails to attend the business meeting of the past year unless excused for justifiable reasons or if a member fails to earn 50 points in the past year, he shall be declared by the Board of Governors to be in inactive status. He shall have until March 15 either to file a letter of excuse or to comply with the required points. </li>
				<li>The rights and privileges shall be restored upon reinstatement to active status as prescribed in this administrative manual. </li>
			</ul>			
		</div>
	</div>
</div>

@endsection