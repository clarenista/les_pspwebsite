Hi <strong>{{ $name }}</strong>, <br>

Your username is <strong><u>{{ $username }}</u></strong>.<br>
To reset your password, click the link below, {{$host}}/membership/passwordreset/{{$prkey}}