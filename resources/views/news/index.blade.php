@extends('app')


@section('content')

<div class="pt-5">
	<div class="container">
		<h1 class="text-success  text-center">News</h1>
		<hr>
		<div class="row justify-content-center">
			<div class="col-10">
				<div class="jumbotron pt-3 pb-0">
					<div class="row justify-content-center">
						<div class="col-12">
							<center>
								
							<img src="{{strlen($latest_news->news_image_filename) > 0 ? $latest_news->news_image_filename : '/uploads/images/logo.jpg'}}" class="img-thumbnail" alt="">									
							</center>
						</div>
						<div class="col-12 pt-4">
							<h3 class="text-success text-center">{{$latest_news->news_title}}</h3>
							<p class="text-center">{!! str_limit(strip_tags($latest_news->news_content), 150) !!}</p>
							<p  class="text-danger text-center">{{\Carbon\Carbon::createFromTimeStamp(strtotime($latest_news->news_timestamp))->toFormattedDateString()}}</p>
							<p class="pb-3 text-center"><strong><a href="/news/{{$latest_news->id}}">READ MORE <span class="text-danger">>></span></a></strong></p>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>
<div class="bg-light">
	<div class="container">
		<div class="row py-3 justify-content-center">
			@foreach ($news as $item)
			    @if ($item->id == $latest_news->id)
			        @continue
			    @endif
				<div class="col-lg-6 col-sm-12 mb-3">
					<div class="card">
						<div class="card-body">
							<div class="row align-items-center p-0">
								<div class="col-5">
									<img src="{{strlen($latest_news->news_image_filename) > 0 ? $latest_news->news_image_filename : '/uploads/images/logo.jpg'}}" class="img-thumbnail" alt="">	
								</div>
								<div class="col-7 mx-auto">
									<h3 class="text-success text-center">{{str_limit($item->news_title,40)}}</h3>
									<p class="text-center">{!! str_limit(strip_tags($item->news_content), 150) !!}</p>
									<p  class="text-danger text-center">{{\Carbon\Carbon::createFromTimeStamp(strtotime($item->news_timestamp))->toFormattedDateString()}}</p>
									<p class="pb-3 text-center"><strong><a href="/news/{{$item->id}}">READ MORE <span class="text-danger">>></span></a></strong></p>
								</div>
							</div>
						</div>
					</div>
				</div>

			@endforeach										
		</div>
	</div>
</div>

@endsection