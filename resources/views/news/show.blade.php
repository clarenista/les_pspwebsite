@extends('app')


@section('content')
<div class="py-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-12 col-sm-12">
				<div class="jumbotron pt-3 pb-0">
					<div class="row justify-content-center">
						<div class="col-lg-12">
							<center>
								
							<img src="{{strlen($news_details->news_image_filename) > 0 ? $news_details->news_image_filename : '/uploads/images/logo.jpg'}}" class="img-thumbnail" alt="">									
							</center>
						</div>
						<div class="col-lg-12 col-sm-12 pt-4">
							<h3 class="text-success text-center">{{$news_details->news_title}}</h3>
							<p  class="text-danger text-center">{{\Carbon\Carbon::createFromTimeStamp(strtotime($news_details->news_timestamp))->toFormattedDateString()}}</p>
							{!! $news_details->news_content !!}
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>

@endsection