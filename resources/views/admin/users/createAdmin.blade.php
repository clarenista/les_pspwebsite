@extends('layout.admin')

@section('content')


    <div class="row">
    <div class="col-lg-6 col-sm-12">
        <h3>Create User</h3>
        
        <form action="{{ route('createAdmin') }}" method="post" >
            @csrf
            <?php 
                $model = $user; 
                $userType = [
                    ['value' => 0, 'label'=> 'Admin'],
                    ['value' => 1, 'label'=> 'Member'],
                ];

            ?>

            @include('admin.include.input-text', ['key' => 'firstName', 'label' => 'First Name'])
            @include('admin.include.input-text', ['key' => 'lastName', 'label' => 'Last Name'])
            @include('admin.include.input-text', ['key' => 'email', 'label' => 'Email Address'])
            @include('admin.include.input-text', ['key' => 'username', 'label' => 'Username'])
            @include('admin.include.input-text', ['key' => 'password', 'label' => 'Password'])
            @include('admin.include.input-select', ['key' => 'type', 'label' => 'Type', 'options' => $userType])
            <br>

            <br>
            <div class="form-group">
                <button class="btn btn-success btn-block">&#10004; ADD ADMIN</button>
            </div>
        </form>
    </div>

</div>
@stop

@section('js')

@stop