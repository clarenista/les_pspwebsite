@extends('layout.admin')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin/users">Users</a></li>
    <li class="breadcrumb-item active">User Details</li>
</ol>

<div class="row">
    <div class="col-lg-4 col-sm-12">
        <div class="card d-flex justify-content-center align-items-center">
            <img class="card-img-top img-fluid mt-2" style="width:50%;" src="/uploads/images/logo.jpg" alt="">
            <div class="card-body text-center">
                <hr>
                <h5 class="card-title">{{$user->firstName}} {{$user->lastName}}</h5>
                <p class="card-text">{{$user->classification}}</p>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-sm-12">
        <div class="card">
            <div class="card-body">
            <button class="btn btn-primary float-right"  data-toggle="modal" data-target="#yearAcceptedModal" type="button">Change year accepted</button>
            <!-- Modal -->
            <div class="modal fade" id="yearAcceptedModal" tabindex="-1" role="dialog" aria-labelledby="yearAcceptedModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Change year accepted</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post">
                        @csrf
                        <div class="form-group">
                            <input id="my-input" class="form-control" type="text" name="year_accepted" value="{{$user['year_accepted']}}">
                        </div>
                        <div class="form-group float-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>

                </div>
            </div>
            </div>
            <table class="table table-light table-responsive">
                <tbody>
                    <tr>
                        <td>Email Address:</td>
                        <td><strong>{{$user->email}}</strong></td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td><strong>{{$user->homeAddress}}, {{$user->homeAddressZipCode}} {{$user->homeAddressCity}}, {{$user->homeAddressCountry}}</strong></td>
                    </tr>
                    <tr>
                        <td>PRC Number:</td>
                        <td><strong>{{$user->prcNumber}}</strong></td>
                    </tr>
                    <tr>
                        <td>PMA Number:</td>
                        <td><strong>{{$user->pmaNumber}}</strong></td>
                    </tr>
                    <tr>
                        <td>Philhealth Accreditation Number:</td>
                        <td><strong>{{$user->philhealth_accreditation_no}}</strong></td>
                    </tr>
                    <tr>
                        <td>Residency Training Institution:</td>
                        <td><strong>{{$user->residency_training_institution}}</strong></td>
                    </tr>
                    <tr>
                        <td>Classification:</td>
                        <td><strong>{{$user->classification}}</strong></td>
                    </tr>
                    <tr>
                        <td>Current Status:</td>
                        <td><strong>{{$user->status}}</strong></td>
                    </tr>
                    <tr>
                        <td>Year Accepted:</td>
                        <td><strong>{{$user->year_accepted}}</strong></td>
                    </tr>
                    <tr>
                        <td>Specialty:</td>
                        <td><strong>{{$user->specialty}}</strong></td>
                    </tr>
                    <tr>
                        <td>Chapter Membership:</td>
                        <td><strong>{{$user->chapter_membership}}</strong></td>
                    </tr>
                    <tr>
                        <td>Area of Practice:</td>
                        <td>
                            <ul>
                                @foreach(explode(',',$user['area_of_practice']) as $area)
                                    <li>{{$area}}</li>
                                @endforeach
                            </ul>   
                        </td>
                    </tr>
                    <tr>
                        <td>Affiliations:</td>
                        <td>
                            <ul>
                                @foreach($user->affiliations as $affiliation)
                                    <li>
                                        <p class="lead mb-0">{{$affiliation->hospital_name}}</p>
                                        <p class="muted mb-0"> Position: {{$affiliation->position}}</p>
                                    </li>
                                @endforeach
                            </ul>   
                        </td>
                    </tr>

                    
                </tbody>

            </table>
            </div>
            
        </div>
    </div>
    
</div>
@stop

@section('js')
<script>

</script>
@stop