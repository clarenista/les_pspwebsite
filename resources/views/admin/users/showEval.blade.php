@extends('layout.admin')

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin/users">Users</a></li>
        <li class="breadcrumb-item active">User Evaluations</li>
    </ol>
    

    <div class="text-center">

        <h3>EVALUATION FORM</h3>
        <h3>69TH ANNUAL CONVENTION</h3>
        <p class="lead">
        “CHARTERING the FUTURE of the PRACTICE of PATHOLOGY and LABORATORY MEDICINE”
            April 23 – 26, 2021

        </p>

        <div class="row">
            <table class="table table-light table-responsive table-striped table-hover">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Rate</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($eval_items as $item)
                        @if($item->day == '1')
                            <tr>
                                <td>{{$item->description}}</td>
                                @foreach((object) $item->user_evaluations as $item)
                                    <td>
                                            <p>{{$item['rate']}}</p>
                                    </td>
                                @endforeach
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            <table class="table table-light table-responsive table-striped table-hover">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Content</th>
                        <th>Adherence</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($eval_items as $item)
                        @if($item->day != '1')
                            <tr>
                                <td>{{$item->description}}</td>
                                @foreach((object) $item->user_evaluations as $item)
                                    <td>
                                            <p>{{$item['rate']}}</p>
                                    </td>
                                @endforeach
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>        
    </div>    
        <div class="card">
            <div class="card-header">
                Comment:
            </div>
            <div class="card-body">
                <p class="card-text">{{$eval_items['comment']['comment']}}</p>
            </div>
        </div>
@stop

@section('js')

@stop