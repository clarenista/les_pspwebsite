@extends('layout.admin')

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Users</li>
    </ol>

    
    <table class="table table-light mp" id="users_table">
        <thead class="thead-light">
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Has Evaluation</th>
                <th nowrap>Action</th>
            </tr >
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{$user->firstName}}</td>
                <td>{{$user->lastName}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->evaluations_count > 0 ? "Yes" : "No"}}</td>
                <td  nowrap>
                    <a href="/admin/users/{{$user->id}}" class="btn btn-info btn-sm">View</a>
                    <a href="/admin/users/{{$user->id}}/paydues" class="btn btn-primary btn-sm">Paydues</a>
                    @if($user->evaluations_count > 0)
                        <a class="btn btn-success float-left btn-sm" href="{{route('showEval', ['id'=>$user->id])}}">Evaluation</a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@stop

@section('js')
<script>
    $(document).ready( function () {
        $('#users_table').DataTable();
    } );
</script>
@stop