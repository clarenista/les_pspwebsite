@extends('layout.admin')

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Applications</li>
    </ol>

    <table class="table table-light">
        <thead class="thead-light">
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Application Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse($users as $user)
            <tr>
                <td>{{$user->firstName}} {{$user->lastName}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->created_at}}</td>
                <td>
                    <button class="btn btn-primary" type="button">PDS</button>
                    <a href="{{$user->certApplications[0]->receipt_path}}" target="_blank" class="btn btn-secondary">Receipt</a>
                    <button class="btn btn-success" type="button">Approve</button>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4"></td>
            </tr>
            @endforelse
        </tbody>
    </table>
    
    
@stop

@section('js')

@stop