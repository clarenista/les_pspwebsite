@extends('layout.admin')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin/users">Users</a></li>
        <li class="breadcrumb-item active">User's Pay dues</li>
    </ol>
    <h3>{{$user->firstName}}'s Pay dues</h3>
    <table class="table table-light" id="dues_table">
        <thead class="thead-light">
            <tr>
                <th>Year</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Receipt</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($user->dues as $due)
                <tr>
                    <td>{{$due->year}}</td>
                    <td>{{$due->total}}</td>
                    <td>{{$due->payment_status}}</td>
                    <td><a target="_blank" href="{{$due->bpifilepath}}">{{$due->bpifilepath}}</a></td>
                    <td>
                        @if($due->payment_status != 'paid')
                            <a class="btn btn-primary" href="{{route('pd_approve',  ['id' => $user->id, 'pd_id' => $due->id])}}">Approve</a>
                        @else
                            <a class="btn btn-danger" href="{{route('pd_approve_revert',  ['id' => $user->id, 'pd_id' => $due->id])}}">Undo approve</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>

    </table>
@stop

@section('js')
<script>
    $(document).ready( function () {
        $('#dues_table').DataTable();
    } );
</script>
@stop