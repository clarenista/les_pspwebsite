<div class="form-group">
    <div class="input-group">
        <span class="input-group-prepend">
            <span class="input-group-text">{{ $label }}</span>
        </span>
        <select id="my-select" class="custom-select" name="{{$key}}">
            @foreach($options as $option)
                <option value="{{$option['value']}}">{{$option['label']}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    @error($key)
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>
