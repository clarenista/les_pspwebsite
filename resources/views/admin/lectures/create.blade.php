@extends('layout.admin')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin/lectures">Lectures</a></li>
        <li class="breadcrumb-item active">Lecture Details</li>
    </ol>

<div class="row">
    <div class="col-lg-6 col-sm-12">
        <h3>Lecture Details</h3>
        
        <form action="{{ route('admin.lectures.store') . ( $lecture->id ? '/' . $lecture->id : '') }}" method="post"
            enctype="multipart/form-data">
            @csrf
            @isset($lecture->id)
            {{ method_field('PUT') }}
            @endisset
            <?php $model = $lecture; ?>

            @include('admin.include.input-text', ['key' => 'title', 'label' => 'Title'])
            @include('admin.include.input-file', ['key' => 'file_path', 'label' => 'Path'])
            <br>

            <br>
            <div class="form-group">
                <button class="btn btn-success btn-block">&#10004; {{isset($lecture->id) ? 'EDIT LECTURE' : 'ADD NEW LECTURE'}}</button>
            </div>
        </form>
    </div>
    <div class="col-lg-6 col-sm-12">
        <h3>Viewers</h3>
        <table class="table table-light table-hover" id="viewers_table">
            <thead class="thead-light">
                <tr>
                    <th>Name</th>
                    <th>View date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($lecture->active_viewers as $viewer)
                <tr>
                    <td>{{$viewer->user->firstName}} {{$viewer->user->lastName}}</td>
                    <td>{{$viewer->created_at}}</td>
                    <td>
                        <a href="{{route('updateIsWatching', ['id' => $viewer->id])}}">test</a>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
</div>
@stop

@section('js')
<script>
    $(document).ready( function () {
        $('#viewers_table').DataTable();
    } );
</script>
@stop