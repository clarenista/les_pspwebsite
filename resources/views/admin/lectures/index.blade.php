@extends('layout.admin')

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Lectures</li>
    </ol>

    <a href="/admin/lectures/create" class="btn btn-success">Add lecture</a>
    <hr>
    <table class="table table-light" id="lectures_table">
        <thead class="thead-light">
            <tr>
                <th>Title</th>
                <th>Path</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($lectures as $lecture)
            <tr>
                <td>{{$lecture->title}}</td>
                <td>{{$lecture->file_path}}</td>
                <td>
                    <a href="{{ route('admin.lectures.edit', $lecture->id) }}" class="btn btn-info">edit</a>
                    <a href="/admin/lectures/{{$lecture->id}}/delete" class="btn btn-info">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@stop

@section('js')
<script>
    $(document).ready( function () {
        $('#lectures_table').DataTable();
    } );
</script>
@stop