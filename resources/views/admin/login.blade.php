@extends('layout.admin')

@section('content')
    <div class="row d-flex justify-content-center ">
        <div class="col-lg-6 col-sm-12">
            <div class="card">
                <div class="card-header">
                    Admin Login
                </div>
                <div class="card-body">
                    <form method="post">
                    @csrf
                        <input class="form-control" type="text" name="email" placeholder="Username">
                        <p></p>
                        <input class="form-control" type="password" name="password" placeholder="Password">
                        <p></p>
                        <button class="btn btn-info" type="submit">Login</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
@stop