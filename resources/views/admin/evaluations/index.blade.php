@extends('layout.admin')

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin/users">Users</a></li>
        <li class="breadcrumb-item active">User Evaluations</li>
    </ol>
    
    <p class="lead text-success">Total number of evaluation forms submitted: <strong>{{$hasEvalCount}}</strong></p>    
    <hr>
    <div class="text-center">

        <h3>EVALUATION (AVERAGE)</h3>
        <h3>69TH ANNUAL CONVENTION</h3>
        <p class="lead">
        “CHARTERING the FUTURE of the PRACTICE of PATHOLOGY and LABORATORY MEDICINE”
            April 23 – 26, 2021

        </p>

        <div class="row">
            <table class="table table-light table-responsive table-striped table-hover">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Rate</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($eval_items as $item)
                        @if($item->day == '1')
                            <tr>
                                <td>{{$item->description}}</td>
                                <td>
                                    <p>{{$item['average']}}</p>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            <table class="table table-light table-responsive table-striped table-hover">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Content</th>
                        <th>Adherence</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($eval_items as $item)
                        @if($item->day != '1')
                            <tr>
                                <td>{{$item->description}}</td>
                                <td>{{$item->content}}</td>
                                <td>{{$item->adherence}}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>        
    </div>    
            <div class="card">
                <div class="card-header">
                    Comments:
                </div>
                <div class="card-body">
                    <ul>
                        @foreach($comments as $comment)
                            @if($comment->comment != '' || $comment->comment != null)
                                <li>{{$comment->comment}}</li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
@stop

@section('js')

@stop