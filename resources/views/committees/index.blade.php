@extends('app')
@section('content')
<div class="bg-light">
	<div class="container py-5">
		<h1 class="text-success text-center">Councils and Committees</h1>
		<hr>
		<div class="row justify-content-center">
			@if(count($councils) > 0)
				@foreach($councils as $council)
					<div class="col-6 py-2">
					  <div class="card shadow " >
					  	<div class="card-header bg-primary text-light rounded-0">
					  		<h4>{{$council->name}} Council</h4>
					  	</div>
					    <div class="card-body">
					    	@if(count($council->committees) > 0)
		    				<h5><i class="fa fa-users"></i> Committees</h5>
		    				<hr>
		    					@foreach($council->committees as $committee)
					    			<div class="card mb-2">
					    				<div class="card-body p-0">
					    					<div class="row justify-content-center align-items-center">
					    						<div class="col-3">
							    					<img class="card-img-top" src="{{$committee->display_picture ? $committee->display_picture : '/images/dp_placeholder.png'}}" alt="Card image">
					    						</div>
					    						<div class="col-9">
							    					<h4 class="card-title mb-0">{{$committee->name}}</h4>
							    					<p class="card-text pb-0">{{$committee->position}}</p>
					    						</div>
					    					</div>
					    				</div>
					    			</div>
		    					@endforeach
	    					@else
								<div class="col">
									<div class="alert alert-dismissible alert-warning">
									  <h4 class="alert-heading">No committees found!</h4>
									  <p>Please contact the Administrator.</p>
									</div>				
								</div>	    						
	    					@endif
					    </div>
					  </div>	
					</div>
				@endforeach		
			@else
				<div class="col">
					<div class="alert alert-dismissible alert-warning">
					  <h4 class="alert-heading">No councils found!</h4>
					  <p>Please contact the Administrator.</p>
					</div>				
				</div>
			@endif
		</div>
	</div>
</div>


@endsection