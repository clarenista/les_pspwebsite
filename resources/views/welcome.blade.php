<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="/css/app.css">

        <title>Philippine Society of Pathologist</title>
        <!-- Styles -->
        <link href="{{ asset('css/apps.css') }}" rel="stylesheet">
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet">        

    </head>
    <body>
            @include('layout.header')
    	   <div id="app"></div>
            @include('layout.footer')            

    	<script type="text/javascript" src="/js/app.js"></script>
    </body>
</html>
