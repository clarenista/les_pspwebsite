@extends('app')


@section('content')
<div class="bg-light">
	<div class="container py-5">
		<h1 class="text-success text-center">Board of Pathology</h1>
		<hr>
		<div class="row justify-content-center">
			@foreach($bops as $bop)
				<div class="col-lg-6 col-sm-12 py-2">
				  <div class="card shadow" >
				    <img class="card-img-top" src="{{$bop->display_picture ? $bop->display_picture : '/images/dp_placeholder.png'}}" alt="Card image">
				    <div class="card-body">
				      <h4 class="card-title text-success">{{$bop->name}}</h4>
				      <p class="card-text">{{$bop->position}}</p>
				    </div>
				  </div>	
				</div>
			@endforeach		
		</div>				
	</div>
</div>


@endsection