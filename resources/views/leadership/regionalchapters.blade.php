@extends('app')


@section('content')
<div class="bg-light">
	<div class="container py-5">
		<h1 class="text-success text-center">Regional Chapters</h1>
		<hr>
			<div class="accordion" id="accordionExample">
				@foreach($regions as $region)
				  <div class="card">
				    <div class="card-header" id="heading{{$region->number}}">
				      <h5 class="mb-0">
				        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$region->number}}" aria-expanded="true" aria-controls="collapse{{$region->number}}">
				          {{$region->name}}
				        </button>
				      </h5>
				    </div>

				    <div id="collapse{{$region->number}}" class="collapse" aria-labelledby="heading{{$region->number}}" data-parent="#accordionExample">
				      <div class="card-body">
				        @foreach($region->leaders as $leader)
				        			<div class="card mb-1">
				        				<div class="card-body p-0">
				        					<div class="row justify-content-center align-items-center">
				        						<div class="col-2">

				        							<img src="{{$leader->display_picture ? $leader->display_picture : '/images/dp_placeholder.png'}}" class="rounded-0 img-fluid img-thumbnail" alt="Responsive image">
				        						</div>
				        						<div class="col-10">
						        					<h4 class="card-title text-primary">{{$leader->name}}</h4>
						        					<p class="card-text text-secondary">{{$leader->position}}</p>
				        						</div>
				        					</div>
				        				</div>
				        			</div>
				        @endforeach
				      </div>
				    </div>
				  </div>
				@endforeach
			</div>
	</div>
	
</div>


@endsection