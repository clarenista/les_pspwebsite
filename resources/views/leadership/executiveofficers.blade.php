@extends('app')
@section('content')
<div class="bg-light">
	<div class="container py-5">
		<h1 class="text-success text-center">Executive Officers</h1>
		<hr>
		<div class="row justify-content-center" >
			@foreach($eos as $eo)
				<div class="col-lg-6 col-sm-12 py-2">
				  <div class="card shadow" >
				    <img class="card-img-top" src="{{$eo->display_picture != null ? $eo->display_picture : '/images/dp_placeholder.png'}}" alt="Card image">
				    <div class="card-body">
				      <h4 class="card-title text-success mb-0">{{$eo->name}}</h4>
				      <hr class="m-1">
				      <p class="card-text lead mb-0">{{$eo->position}}</p>
				      <p>{{$eo->term}}</p>
				    </div>
				  </div>	
				</div>
			@endforeach	
		</div>
	</div>
</div>
@endsection