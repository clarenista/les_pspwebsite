@extends('app')
@section('content')
<div class="bg-light">
	<div class="container py-5">
		<h1 class="text-success text-center">Board of Governors</h1>
		<hr>
		<div class="row justify-content-center">
			@foreach($bogs as $bog)
				<div class="col-lg-6 col-sm-12 py-2">
				  <div class="card shadow" >
				    <img class="card-img-top" src="{{$bog->display_picture != null ? $bog->display_picture : '/images/dp_placeholder.png'}}" alt="Card image">
				    <div class="card-body">
				      <h4 class="mb-0 card-title text-success">{{$bog->name}}</h4>
				      <hr class="m-0">
				      <p class="mb-0 card-text">{{$bog->position}}</p>
				      <p class="mb-0 card-text">{{$bog->term}}</p>
				    </div>
				  </div>	
				</div>
			@endforeach		
		</div>
	</div>
</div>


@endsection