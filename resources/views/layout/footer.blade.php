<!-- Footer -->
<div class="container">
  
  <div class="row py-5">
    <div class="col d-none d-lg-block">
      <p class="text-success">Philippine Society of Pathologist</p>
    </div>

    <!-- Contact Us -->
    <div class="col">
      <h3 class="text-dark">Contact us</h3>
      <hr>
      <p><i class="fa fa-map-marker-alt"></i> #114 Malakas Street, Diliman, Quezon City Philippines</p>
      <p><i class="fa fa-phone"></i> (+632) 738-6814</p>
      <p><i class="fa fa-fax"></i> (+632) 920-3192</p>
    </div>

    <!-- Quick Links -->
    <div class="col">
      <h3 class="text-dark">Quick Links</h3>
      <hr>
      <p><a href="/discussions">Discussions</a></p>
      <p><a href="/news">News, Events & Announcements</a></p>
    </div>    
  </div>
</div>    
