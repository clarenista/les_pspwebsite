<div class="row justify-content-center " style="background-color: #71bc55; margin-left: 0px !important; margin-right: 0px !important; ">
  <div id="logo">
    <img src="https://psp.com.ph/images/bannerlogo4.jpg" alt="banner" class="img-fluid">
  </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark bg-success sticky-top" id="top-nav">
  <div class="container">
    
    <a class="navbar-brand" href="/">PSP</a>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse" id="navbarColor01" style="">
      <ul class="navbar-nav mr-auto justify-content-center">

      </ul>
      <form class="form-inline my-2 my-lg-0">
      <ul class="navbar-nav mr-auto justify-content-center">

        <li class="nav-item">
          <a class="nav-link" href="/about-us">About us</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">Leadership</a>
          <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 40px, 0px);">
            <a class="dropdown-item" href="/leadership/executiveofficers">Executive Officers</a>
            <a class="dropdown-item" href="/leadership/boardofgovernors">Board of Governors</a>
            <a class="dropdown-item" href="/leadership/boardofpathology">Board of Pathology</a>
            <a class="dropdown-item" href="/leadership/regionalchapters">Regional Chapters</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/councilAndCommittees">Council & Committees</a>
        </li>


        <li class="nav-item">
          <a class="nav-link" href="/accreditation">Accreditation</a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" href="http://philippinejournalofpathology.org/index.php/PJP" target="_blank" rel="noopener noreferrer">Publication</a>
        </li>   
        
        <li class="nav-item">
          <a class="nav-link" href="/discussions">Discussions</a>
        </li>       
        <li class="nav-item">
          <a class="nav-link" href="/news">News</a>
        </li>  
        <li class="nav-item">
          <a class="nav-link" href="/membership/login">Membership Login</a>
        </li>   
      </ul>
      </form>      

    </div>
  </div>
</nav>
