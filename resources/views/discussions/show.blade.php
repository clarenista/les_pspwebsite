@extends('app')


@section('content')

<div class="bg-light py-5">
	<div class="container">
		<hr>
		<div class="row">
			<div class="col">
				<img src="{{strlen($discussion_details->discussion_image_filename) > 0 ? $discussion_details->discussion_image_filename : '/uploads/images/logo.jpg'}}" class="rounded mx-auto d-block" alt="">
				<br>
				<p  class="text-danger">{{\Carbon\Carbon::createFromTimeStamp(strtotime($discussion_details->discussion_timestamp))->toFormattedDateString()}}</p>
				<h1>{{$discussion_details->discussion_title}}</h1>
				<p>{!! $discussion_details->discussion_content !!}</p>

				<h3>Comments</h3>
				<hr>
				@foreach($discussion_details->comments as $comment)
					<p class="lead">{{$comment->comment}}</p>
					<p class="text-success">&nbsp-{{$comment->name}} <i>posted on {{\Carbon\Carbon::createFromTimeStamp(strtotime($comment->created_at))->toFormattedDateString()}}</i></p>
				<hr>

				@endforeach

				<div class="row">
					<div class="col-lg-5 col-sm-12">
						<div class="mt-5">
							<h3>Leave a Reply</h3>
							<p>Your email address will not be published. Required fields are marked *</p>
						    <div class="form-group">
						      <label for="comment">Comment</label>
						      <textarea class="form-control form-control-lg" id="comment" name="comment" rows="10" required=""></textarea>
						    </div>
						    <div class="form-group">
						      <label for="name">Name *</label>
						      <input type="text" class="form-control form-control-lg" name="name" required="" />
						    </div>
						    <div class="form-group">
						      <label for="email">Email *</label>
						      <input type="email" class="form-control form-control-lg" name="email" required="" />
						    </div>
						    <div class="form-group">
								<button type="submit" onclick="javascript.void(0)" class="btn btn-success"><p class="text-uppercase">Post comment</p></button>					    	
						    </div>							    					    										    
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

@endsection