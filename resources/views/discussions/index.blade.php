@extends('app')


@section('content')

<div class="bg-light py-5">
	<div class="container">
		<h1 class="text-success pb-5 text-center">Discussions</h1>
		<hr>
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				
				@foreach($discussions as $discussion)
					<img src="{{strlen($discussion->discussion_image_filename) > 0 ? $discussion->discussion_image_filename : '/uploads/images/logo.jpg'}}" class="rounded mx-auto d-block" alt="">
					<br>
					<a href="/discussions/{{$discussion->id}}"><p  class="text-danger">{{\Carbon\Carbon::createFromTimeStamp(strtotime($discussion->discussion_timestamp))->toFormattedDateString()}}</p></a>
					<a href="/discussions/{{$discussion->id}}"><h1>{{$discussion->discussion_title}}</h1></a>
					<p>{!! str_limit(strip_tags($discussion->discussion_content), 150) !!}</p>

					<p class="pb-3"><strong><a href="/discussions/{{$discussion->id}}">READ MORE <span class="text-danger">>></span></a></strong></p>
					<hr>
				@endforeach
			</div>
			<div class="col-4 d-none d-lg-block">
				<div class="card border-light mb-3" style="max-width: 20rem;">
				  <div class="card-body">
				    <h4 class="card-title">Recent post</h4>
				    <hr>
					<div class="list-group">
						@foreach($right_pane as $item)
						<a href="/discussions/{{$item->id}}" class="list-group-item list-group-item-action flex-column align-items-start">
							<div class="d-flex w-100 justify-content-between">
							  <h5 class="mb-1">{{$item->discussion_title}}</h5>
							  <small>{{\Carbon\Carbon::createFromTimeStamp(strtotime($item->discussion_timestamp))->diffForHumans()}}</small>
							</div>
						</a>

						@endforeach
					</div>				    
				  </div>
				</div>
			</div>
		</div>
	</div>

</div>

@endsection