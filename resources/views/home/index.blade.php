@extends('app')


@section('content')
<!-- 
<div class="bg-info p-3 text-center">
		
	<a target="_blank" href="https://eventsv2.psp.com.ph/MolPath-online-course" class=" text-light">		<i class="fa fa-bullhorn"></i> Registration to PSP Online Course on Molecular Pathology  - Batch 3 August 20 to 26, 2020
</a>
</div>
-->
<a href="https://psp69ac.psp.com.ph/" target="_blank">
	<div id="demo" class="carousel slide " data-ride="carousel" >

	  <div class="carousel-inner ">
	    <div class="carousel-item active ">
	    	<center>

	      		<img src="{{$promoting_banner->url ? $promoting_banner->url : ''}}" alt="Promoting photo" class="img-fluid">
	    	</center>
	    </div>
	  </div>
	</div>
</a>


<!-- News Row -->
<div class="bg-light">
	<div class="h-100 w-100 p-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col py-5">
					<h1 class="text-success">Latest Discussions</h1>
					<hr>

					<h5>{{$latest_discussion->discussion_title}}</h5>
					<p class="text-dark">{!! str_limit(strip_tags($latest_discussion->discussion_content), 150) !!}.</p>
					<div class="p-0">
						
						<a href="/discussions/{{$latest_discussion->id}}" class="btn btn-outline-success"><p>Read More...</p></a>
					</div>
				</div>

				<div class="col d-none d-lg-block">
				  	<div class="p-0 justify-content-center">
		    	        <div class="jumbotron jumbotron-fluid p-0">
		    	        	<img src="https://static.wixstatic.com/media/7b6489_b80bd821036b47ba895ec68f847d24ac~mv2.png" class="h-100 w-100 rounded shadow" alt="">
		    			</div>
				  	</div>				
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Video Row -->
<!-- Video Modal -->
<div class="modal " id="videoModal">
  <div class="modal-dialog modal-lg " role="document">
    <div class="modal-content ">
      <div class="modal-body">
      	<div class="row justify-content-center">
			<iframe id="videoIFrame" src="" data-autoplay="true" width="800" height="470" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnCloseVidModal" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="bg-success">
	<div class="container">
		<div style="height: 300px;">
			<div class="row h-100">
				
				<div class="col-sm-12 my-auto">
					<h2 class="mx-auto text-light text-uppercase text-center">{{$promoting_video->title}}</h2>
					<div class="mx-auto text-center">
						<button id="btnShowVidModal" class=" btn btn-outline-light rounded-circle btn-lg" data-toggle="modal" data-target="#videoModal"><i class="fas fa-play"></i></button>
					</div>
				</div>
			</div>
		</div>
	</div>		
</div>


<!-- memPortal Row -->
<div class="bg-light">
	<div class="container">
		
		<div class="row p-5">
			<!-- MEMBER PORTAL ICON -->
			<div class="col-sm-12 col-lg-4 mb-3">
				<a href="/membership/login">
				  <div class="card">
				  	<div class="p-2 text-center">
				    	<i class="fas fa-sign-in-alt fa-5x text-success icon-gradient"></i>
				  	</div>
				  	<div class="container">
				  		<hr>
				  	</div>
				    <div class="card-body">
				      <h3 class="card-title text-center">Membership Portal</h3>
				      <p class="text-center">View your profile.</p>
				  	</div>	
				  </div>	
				</a>
			</div>

			<!-- EVENTS ICON -->
			<div class="col-sm-12 col-lg-4 mb-3">
				<a href="http://events.psp.com.ph" target="_blank">
				  <div class="card">
				  	<div class="p-2 text-center">
				    	<i class="fas fa-calendar-alt fa-5x text-success icon-gradient"></i>
				  	</div>
				  	<div class="container">
				  		<hr>
				  	</div>			  	
				    <div class="card-body">
				      <h3 class="card-title text-center">Events</h3>
				      <p class="text-center">PSP Event Manager</p>
				  	</div>	
				  </div>	
				</a>
			</div>

			<!-- PJP ICON -->
			<div class="col-sm-12 col-lg-4 mb-3">
				<a href="http://philippinejournalofpathology.org/index.php/PJP" target="_blank">
				  <div class="card">
				  	<div class="p-2 text-center">
				    	<i class="fas fa-microscope fa-5x text-success icon-gradient"></i>
				  	</div>
				  	<div class="container">
				  		<hr>
				  	</div>			  	
				    <div class="card-body">
				      <h3 class="card-title text-center">PJP</h3>
				      <p class="text-center">Philippine Journal of Pathology</p>
				  	</div>	
				  </div>	
				</a>
			</div>		
		</div>
	</div>		
</div>


<!-- Discussion Row -->
<div class="">
	<div class="h-100 w-100 p-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-6">
					<h1 class="text-success text-center">News</h1>
					<hr>
					<p class="text-dark text-center">Latest news in forums.</p>
				</div>
			</div>
			<div class="row justify-content-center">
				@foreach($news as $news_item)
					<div class="col-sm-12 col-lg-4 mb-3">
					  <div class="card border-secondary">
					    <div class="card-body">
							<p  class="text-danger">{{\Carbon\Carbon::createFromTimeStamp(strtotime($news_item->news_timestamp))->toFormattedDateString()}}</p>					    	
					      <h4 class="card-title text-success">{{$news_item->news_title}}</h4>
					      <hr>
					      <p class="card-text">{!! str_limit(strip_tags($news_item->news_content), 150) !!}</p>
					      <p class="pb-3 text-center"><strong><a class="btn btn-outline-success rounded" href="/news/{{$news_item->id}}">Read More</a></strong></p>	
					    </div>
					  </div>					
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>


<!-- Follow us -->
<div class=" bg-success">
	<div class="container">
		<div class="p-5" style="height: 300px;">
			<div class="row h-100">
				<div class="col-sm-12 my-auto">
					<h2 class="text-center text-light">Follow us on <a target="_blank" href="https://www.facebook.com/Philippine-Society-of-Pathologists-232421263467403/" class="socialMediaBtn"><i class="fab fa-facebook fa-3x text-light "></i></a> 
					
				</div>
				</h2>
			</div>
		</div>
	</div>		
</div>


<script>
	window.onload = function(){
		$( ".card" ).hover(
			function() {
				$(this).addClass('shadow').css('cursor', 'pointer'); 
				}, function() {
					$(this).removeClass('shadow');
			}
		);

	    $( ".socialMediaBtn" ).hover(
		  function() {
		    $(this).addClass('shadow').css('cursor', 'pointer'); 
		  }, function() {
		    $(this).removeClass('shadow');
		  }
		)

		$('#videoModal').on('shown.bs.modal', function () {
			$('#videoIFrame').attr('src', '{{$promoting_video->url ? $promoting_video->url : ''}}')
		})

		$('#videoModal').on('hide.bs.modal', function () {
			$('#videoIFrame').attr('src','')
		})


	}
</script>

@endsection