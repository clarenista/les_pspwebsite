import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    user: null,
    isEvaluate: false,
    convention_site_eval: {
      count : 5, 
      items : [
        // {'id': 1, 'item' : "REGISTRATION"},
        // {'id': 2,'item' : "VISUAL PRESENTATION"},
        // {'id': 3, 'item' : "OVER-ALL PROGRAM FLOW"},
      ]      
    },
    day_1_eval: {
      count: 5,
      items:[
        // {'id': 4, 'item' : "Opening Ceremonies"},
        // {'id': 5, 'item' : "Professional Fees"},
        // {'id': 6, 'item' : "DOH Hour"},
        // {'id': 7, 'item' : "Business Hour / Resident’s Hour"},
        // {'id': 8, 'item' : "Fellowship Night"},
        // {'id': 9, 'item' : "Closing Cermonies"},
      ]
    },
    lecture_eval: {
      count: 2,
      items:[
        // {'id': 10, 'item' : "Getting the Most out of Proficiency Testing Dr. Bharati Jhaveri", 'day': 2},
        // {'id': 11, 'item' : "Role of the Laboratory During Pandemic: Contributing to Care without Sacrificing -Dr. Bharati Jhaveri	", 'day': 2},
        // {'id': 12, 'item' : "Solving the Puzzle of Unexplained Outbreaks with a Multidisciplinary Approach - Dr. Wun-Ju Shieh	", 'day': 2},
        // {'id': 13, 'item' : "Pathology and Pathogenesis of COVID-19 Dr. Wun-Ju Shieh	", 'day': 2},
        // {'id': 14, 'item' : "Pooled Testing of Covid Dr. Raymundo Lo	", 'day': 2},
        // {'id': 15, 'item' : "Fatty Liver and your Blood Tests Dr. Rouchelle Dela Cruz	", 'day': 2},
        // {'id': 16, 'item' : "Decreasing Blood Pathogens in Transfusion Dr. Joaquin Patag	", 'day': 2},
        // {'id': 17, 'item' : "10 Things Blood Bankers wish Everybody Knew Dr. W. Tait Stevens	", 'day': 2},
        // {'id': 18, 'item' : "Histocompatibility for Pathologists Dr. W. Tait Stevens	", 'day': 2},
        // {'id': 19, 'item' : "Stem Cell Testing: Current Applications Dr. Arvin Faundo	", 'day': 2},
        // {'id': 20, 'item' : "Marsman_A Menu That Matters for Covid-19 Dr. Tze Wei Poh	", 'day': 2},
        // {'id': 21, 'item' : "Lifeline_Detection of COVID Immunity through its Native Trimeric Form: Advantage Over Variants and in Vaccination - Dr. Andrea Goh	", 'day': 2},
        // {'id': 22, 'item' : "Mindray_Neutralizing Antibody in COVID-19 Dr. Tina Liu	", 'day': 2},
        // {'id': 24, 'item' : "Vazyme_RBD Antibody Testing with high-throughput and Open System sVNT Dr. Ke Ma	", 'day': 2},
        // {'id': 25, 'item' : "Integrating Imaging, Histology, Immunohistochemistry and Molecular Information In The Diagnosis of Central Nervous System Tumors - Dr. Leomar Y. Ballester	", 'day': 3},
        // {'id': 26, 'item' : "Update In Pediatric Brain Tumors Dr. Jose Enrique Velazquez Vega	", 'day': 3},
        // {'id': 27, 'item' : "The Role of CSF Biomarkers In The Diagnosis of Patients with Central Nervous System Malignancies - Dr. Leomar Y. Ballester	", 'day': 3},
        // {'id': 28, 'item' : "HPV-Associated Lesions of the Anal Canal Dr. Lysandra Voltaggio	", 'day': 3},
        // {'id': 29, 'item' : "Gastritis: A Pattern Based Approach Dr. Lysandra Voltaggio	", 'day': 3},
        // {'id': 30, 'item' : "Practical Approach in Diagnosis and Biomarker of Cutaneous Melanocytic Lesions - Dr. Phyu P Aung	", 'day': 3},
        // {'id': 31, 'item' : "PD-L1 IHC 22C3 pharmDx: The Pathologist’s Key Role in Predictive Biomarker Testing for Immunotherapy - Dr. Jasper Andal	", 'day': 3},
        // {'id': 32, 'item' : "Abbott_Point of Care Testing: The Cutting Edge in Laboratory Diagnostics in the 21st Century - Dr. Frederick Llanera	", 'day': 3},
        // {'id': 33, 'item' : "Biosite_Introduction to Tosoh G11 (HbA1c & B-thal) Dr. Cheelun Lim	", 'day': 3},
        // {'id': 34, 'item' : "A Practical Approach to Skin Adnexal Tumors Dr. Celestine Marie G. Trinidad	", 'day': 4},
        // {'id': 35, 'item' : "IAC Yokohama System Reporting on Breast Cytopathology Dr. Monalyn T. Marabi	", 'day': 4},
        // {'id': 36, 'item' : "Some Applications of Molecular Tests in the Diagnosis of Bone and Soft Tissue Tumors - Dr. Eugene G.Odoño	", 'day': 4},
        // {'id': 37, 'item' : "cIMPACT-NOW and Its Impact on the upcoming 2021 WHO Classification for CNS Tumors - Dr. Justine Alessandra Uy	", 'day': 4},
        // {'id': 38, 'item' : "Medtek_Monocyte Distribution Width for Improved Sepsis Detection Tze Wei Poh	", 'day': 4},
        // {'id': 39, 'item' : "Arkray_Emerging Trends in HbA1c Analysis in POCT Capillary Electrophoresis, a new player - John Carlo H. Lee, Rph	", 'day': 4},
        // {'id': 40, 'item' : "Ortho_Procalcitonin: An effective Biomarker in the Management of Sepsis Patients - Ron Carlton, Phd, MT (ASCP)	", 'day': 4},
      ]
    },
    isLectureDestroyed: false
      


  },
  mutations: {
    //   this.$store.commit('change', event.target.value)
    updateUser(state, user) {
      state.user = user
    },
    updateEvaluate(state, isEvaluate){
      state.isEvaluate = isEvaluate
    },
    updateEvaluationItems(state, items){
      for(let i in items){
        if(items[i].id <= 3){
          state.convention_site_eval.items.push(items[i]);
        }else if(items[i].id >= 3 && items[i].id <= 9){
          state.day_1_eval.items.push(items[i]);
        }else if(items[i].id >= 9 && items[i].id <= 39){
          state.lecture_eval.items.push(items[i]);
          
        }
      }
    },
    updateIsLectureDestroyed(state, items){
      state.isLectureDestroyed = isLectureDestroyed
    }
  },
  getters: {
    user: state => state.user,
    isEvaluate: state => state.isEvaluate,
    convention_site_eval: state => state.convention_site_eval,
    day_1_eval: state => state.day_1_eval,
    lecture_eval: state => state.lecture_eval,
    isLectureDestroyed: state => state.isLectureDestroyed,
  },
  actions:{
    async getUser({commit}){
      try{
        let {data} = await axios.get('/api/user?api_token='+localStorage.getItem('access_token'));
        commit('updateUser', data.user)
        if(data.evaluations != ''){
          commit('updateEvaluate', true)
        }
        // if(localStorage.getItem('access_token')){
          // }
      }catch({response}){
        // alert(response.statusText)
      }
    },
    async addLectureViewer({commit}, id){
      try{
        let fd = new FormData()
        fd.append('lecture_id', id)
        let {data} = await axios.post('/api/lectures/'+id+'/add-view?api_token='+localStorage.getItem('access_token'), fd);
      }catch({response}){
        alert(response.statusText)
      }
    },
    async removeLectureViewer({commit}, id){
      try{
        let fd = new FormData()
        fd.append('lecture_id', id)
        let {data} = await axios.post('/api/lectures/'+id+'/remove-view?api_token='+localStorage.getItem('access_token'), fd);
        commit('updateIsLectureDestroyed', true)
      }catch({response}){
        alert(response.statusText)
      }
    }
  }
})