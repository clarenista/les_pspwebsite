import 'es6-promise/auto'
import axios from 'axios'
import './bootstrap'
import Vue from 'vue'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import auth from './auth'
import Index from './Index'
import router from './routes'
import { store } from './store/store'

// Set Vue globally
window.Vue = Vue

// Set Vue router
Vue.router = router
Vue.use(VueRouter)

// Set Vue authentication
Vue.use(VueAxios, axios)
// axios.defaults.baseURL = 'http://localhost:8000/api';
// axios.defaults.baseURL = 'https://psp.com.ph/api';
// Vue.use(VueAuth, auth)

// Vue.prototype.$appToken = window.localStorage.getItem('laravel-vue-spa');
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

// Vue.component('index', Index)?

const app = new Vue({
    el: '#app',
    router,
    store,
    render: app => app(Index),
    created() {
        this.init()
    },
    methods:{
        async init(){
            
            // console.log(data)
        }

    }
});
