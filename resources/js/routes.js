import VueRouter from 'vue-router';
import { store } from './store/store'

import Home from './components/Home'
import Login from './components/Login'
import PasswordRemind from './components/PasswordRemind'
import PasswordReset from './components/PasswordReset'
import Dashboard from './components/Dashboard'
import Credentials from './components/Credentials'
import PayDues from './components/PayDues'
import Lectures from './components/Lectures'
import Lecture from './components/Lecture'
import UploadReceipt from './components/UploadReceipt'
import LectureView from './components/LectureView'
import LectureExam from './components/LectureExam'
import Evaluation from './components/Evaluation'
import GoodStanding from './components/GoodStanding'

const routes = [
	{
		path: '/membership/login',
		name: 'login',
		component: Login,
		meta: {
			requiresAuth: false
		},
	},	

	{
		path: '/membership/passwordremind',
		name: 'passwordRemind',
		component: PasswordRemind,
		meta: {
			auth:false
		},
	},	

	{
		path: '/membership/passwordreset/:prkey',
		name: 'passwordReset',
		component: PasswordReset,
		meta: {
			auth:false
		},
	},		

	{
		path: '/membership/dashboard',
		name: 'dashboard',
		component: Dashboard,
		meta: {
			requiresAuth: true
		}
	},		
	{
		path: '/membership/credentials',
		name: 'credentials',
		component: Credentials,
		meta: {
			requiresAuth: true
		}
	},	
	{
		path: '/membership/paydues',
		name: 'paydues',
		component: PayDues,
		meta: {
			requiresAuth: true
		}
	},	

	{
		path: '/membership/lectures',
		name: 'lectures',
		component: Lectures,
		meta: {
			requiresAuth: true
		}
	},	
	{
		path: '/membership/lectures/buy/:id',
		name: 'lecture',
		component: Lecture,
		meta: {
			requiresAuth: true
		}
	},		
	{
		path: '/membership/lectures/:id/uploadReceipt',
		name: 'uploadReceipt',
		component: UploadReceipt,
		meta: {
			requiresAuth: true
		}
	},	
	{
		path: '/membership/lectures/:id/view',
		name: 'lectureView',
		component: LectureView,
		meta: {
			requiresAuth: true
		}
	},		
	{
		path: '/membership/lectures/:id/exam',
		name: 'lectureExam',
		component: LectureExam,
		meta: {
			requiresAuth: true
		}
	},		
	{
		path: '/membership/executiveOfficer',
		name: 'executiveOfficer',
		component: LectureExam,
		meta: {
			requiresAuth: true
		}
	},	
	{
		path: '/membership/evaluation',
		name: 'evaluation',
		component: Evaluation,
		beforeRouteEnter (to, from, next) {
			store.dispatch("getUser");
		  },
		meta: {
			requiresAuth: true
		}
	},	
	{
		path: '/membership/application/good-standing',
		name: 'goodStanding',
		component: GoodStanding,
		beforeRouteEnter (to, from, next) {
			store.dispatch("getUser");
		  },
		meta: {
			requiresAuth: true
		}
	},		
]
const router = new VueRouter({
    mode: 'history',
    routes: routes
});

router.beforeEach((to, from, next) => {
    // console.log(to.matched.some(record => record.meta.requireCanCreateUser))
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('access_token') == null) {
            next({
                name: 'login'
            })
        } else {
			if(store.getters.user){
				next()
				
			}else{
				next({
					name: 'login'
				})

			}
            if (to.matched.some(record => record.meta.requireCanCreateUser)) {
                if (store.getters.permissions.includes('manage user')) {
                    next()
                } else {
                    next({
                        name: 'notFound'
                    })
                }
            } else if (to.matched.some(record => record.meta.requireCanManageBooth)) {
                if (store.getters.permissions.includes('manage booth')) {
                    next()
                } else {
                    next({
                        name: 'notFound'
                    })
                }
            }

            else {
                next()
            }
                next()
        }
        // next()
    } else {
        next()
    }
});

export default router;
