<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');
        // app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'view applicants']);
        Permission::create(['name' => 'approve applicants']);

        // create roles and assign existing permissions
        $panel = Role::create(['name' => 'panel']);
        $panel->givePermissionTo('view applicants');
        $panel->givePermissionTo('approve applicants');

        // $role3 = Role::create(['name' => 'Super-Admin']);
        // // gets all permissions via Gate::before rule; see AuthServiceProvider

        // create users
        $user = \App\User::create([
            'name' => 'Example User',
            'email' => 'test@example.com',
            'username' => 'panel1',
            'password' => bcrypt('123'),
            'role' => 0,
        ]);
        $user->assignRole($panel);

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Example Admin User',
        //     'email' => 'admin@example.com',
        // ]);
        // $user->assignRole($role2);

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Example Super-Admin User',
        //     'email' => 'superadmin@example.com',
        // ]);
        // $user->assignRole($role3);
    }
}
