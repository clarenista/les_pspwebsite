<?php

use Illuminate\Database\Seeder;
use App\EvaluationItem;

class EvaluationItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $items = [
            ["REGISTRATION",1],
            ["VISUAL PRESENTATION",1],
            ["OVER-ALL PROGRAM FLOW",1],
            ["Opening Ceremonies",1],
            ["Professional Fees",1],
            ["DOH Hour",1],
            ["Business Hour / Resident’s Hour",1],
            ["Fellowship Night",1],
            ["Closing Cermonies",1],
            ["Getting the Most out of Proficiency Testing Dr. Bharati Jhaveri",2],
            ["Role of the Laboratory During Pandemic: Contributing to Care without Sacrificing -Dr. Bharati Jhaveri	",2],
            ["Solving the Puzzle of Unexplained Outbreaks with a Multidisciplinary Approach - Dr. Wun-Ju Shieh	",2],
            ["Pathology and Pathogenesis of COVID-19 Dr. Wun-Ju Shieh	",2],
            ["Pooled Testing of Covid Dr. Raymundo Lo	",2],
            ["Fatty Liver and your Blood Tests Dr. Rouchelle Dela Cruz	",2],
            ["Decreasing Blood Pathogens in Transfusion Dr. Joaquin Patag	",2],
            ["10 Things Blood Bankers wish Everybody Knew Dr. W. Tait Stevens	",2],
            ["Histocompatibility for Pathologists Dr. W. Tait Stevens	",2],
            ["Stem Cell Testing: Current Applications Dr. Arvin Faundo	",2],
            ["Marsman_A Menu That Matters for Covid-19 Dr. Tze Wei Poh	",2],
            ["Lifeline_Detection of COVID Immunity through its Native Trimeric Form: Advantage Over Variants and in Vaccination - Dr. Andrea Goh	",2],
            ["Mindray_Neutralizing Antibody in COVID-19 Dr. Tina Liu	",2],
            ["Vazyme_RBD Antibody Testing with high-throughput and Open System sVNT Dr. Ke Ma	",2],
            ["Integrating Imaging, Histology, Immunohistochemistry and Molecular Information In The Diagnosis of Central Nervous System Tumors - Dr. Leomar Y. Ballester	",3],
            ["Update In Pediatric Brain Tumors Dr. Jose Enrique Velazquez Vega	",3],
            ["The Role of CSF Biomarkers In The Diagnosis of Patients with Central Nervous System Malignancies - Dr. Leomar Y. Ballester	",3],
            ["HPV-Associated Lesions of the Anal Canal Dr. Lysandra Voltaggio	",3],
            ["Gastritis: A Pattern Based Approach Dr. Lysandra Voltaggio	",3],
            ["Practical Approach in Diagnosis and Biomarker of Cutaneous Melanocytic Lesions - Dr. Phyu P Aung	",3],
            ["PD-L1 IHC 22C3 pharmDx: The Pathologist’s Key Role in Predictive Biomarker Testing for Immunotherapy - Dr. Jasper Andal	",3],
            ["Abbott_Point of Care Testing: The Cutting Edge in Laboratory Diagnostics in the 21st Century - Dr. Frederick Llanera	",3],
            ["Biosite_Introduction to Tosoh G11 (HbA1c & B-thal) Dr. Cheelun Lim	",3],
            ["A Practical Approach to Skin Adnexal Tumors Dr. Celestine Marie G. Trinidad	",4],
            ["IAC Yokohama System Reporting on Breast Cytopathology Dr. Monalyn T. Marabi	",4],
            ["Some Applications of Molecular Tests in the Diagnosis of Bone and Soft Tissue Tumors - Dr. Eugene G.Odoño	",4],
            ["cIMPACT-NOW and Its Impact on the upcoming 2021 WHO Classification for CNS Tumors - Dr. Justine Alessandra Uy	",4],
            ["Medtek_Monocyte Distribution Width for Improved Sepsis Detection Tze Wei Poh	",4],
            ["Arkray_Emerging Trends in HbA1c Analysis in POCT Capillary Electrophoresis, a new player - John Carlo H. Lee, Rph	",4],
            ["Ortho_Procalcitonin: An effective Biomarker in the Management of Sepsis Patients - Ron Carlton, Phd, MT (ASCP)	",4]
        ];

        foreach ($items as $item) {
            EvaluationItem::create([
                'description' => $item[0],
                'day' => $item[1] ? $item[1] : null,

            ]);
        }
    }
}
