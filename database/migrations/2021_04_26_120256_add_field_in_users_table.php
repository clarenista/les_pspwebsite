<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('philhealth_accreditation_no')->nullable();
            $table->string('residency_training_institution')->nullable();
            $table->string('classification')->nullable();
            $table->string('status')->nullable();
            $table->string('year_accepted')->nullable();
            $table->string('specialty')->nullable();
            $table->string('chapter_membership')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
